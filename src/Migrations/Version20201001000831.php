<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201001000831 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE mapa_produto ADD calibre_id INT NOT NULL');
        $this->addSql('ALTER TABLE mapa_produto ADD CONSTRAINT FK_ACE585CD58FEF8CD FOREIGN KEY (calibre_id) REFERENCES mapa_calibre (id)');
        $this->addSql('CREATE INDEX IDX_ACE585CD58FEF8CD ON mapa_produto (calibre_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE mapa_produto DROP FOREIGN KEY FK_ACE585CD58FEF8CD');
        $this->addSql('DROP INDEX IDX_ACE585CD58FEF8CD ON mapa_produto');
        $this->addSql('ALTER TABLE mapa_produto DROP calibre_id');
    }
}
