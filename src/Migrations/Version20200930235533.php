<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200930235533 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE mapa_calibre (id INT AUTO_INCREMENT NOT NULL, descricao VARCHAR(40) NOT NULL, tipo_calibre VARCHAR(40) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mapa_cliente_fornecedor (id INT AUTO_INCREMENT NOT NULL, cpf VARCHAR(14) NOT NULL, nome VARCHAR(40) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mapa_estoque (id INT AUTO_INCREMENT NOT NULL, calibre_id INT NOT NULL, mes INT NOT NULL, ano INT NOT NULL, qtde_inicial INT DEFAULT NULL, entrada INT NOT NULL, saida INT NOT NULL, qtde_final INT NOT NULL, INDEX IDX_6CA9B41058FEF8CD (calibre_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mapa_movimento (id INT AUTO_INCREMENT NOT NULL, cliente_fornecedor_id INT NOT NULL, produto_id INT NOT NULL, mes INT NOT NULL, ano INT NOT NULL, qtde INT NOT NULL, registro VARCHAR(80) NOT NULL, doc_fiscal VARCHAR(15) NOT NULL, INDEX IDX_791F6D1CF6D068FB (cliente_fornecedor_id), INDEX IDX_791F6D1C105CFD56 (produto_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mapa_parametro (id INT AUTO_INCREMENT NOT NULL, mes_aberto INT NOT NULL, ano_aberto INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mapa_produto (id INT AUTO_INCREMENT NOT NULL, codigo VARCHAR(15) NOT NULL, descricao VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE mapa_estoque ADD CONSTRAINT FK_6CA9B41058FEF8CD FOREIGN KEY (calibre_id) REFERENCES mapa_calibre (id)');
        $this->addSql('ALTER TABLE mapa_movimento ADD CONSTRAINT FK_791F6D1CF6D068FB FOREIGN KEY (cliente_fornecedor_id) REFERENCES mapa_cliente_fornecedor (id)');
        $this->addSql('ALTER TABLE mapa_movimento ADD CONSTRAINT FK_791F6D1C105CFD56 FOREIGN KEY (produto_id) REFERENCES mapa_produto (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE mapa_estoque DROP FOREIGN KEY FK_6CA9B41058FEF8CD');
        $this->addSql('ALTER TABLE mapa_movimento DROP FOREIGN KEY FK_791F6D1CF6D068FB');
        $this->addSql('ALTER TABLE mapa_movimento DROP FOREIGN KEY FK_791F6D1C105CFD56');
        $this->addSql('DROP TABLE mapa_calibre');
        $this->addSql('DROP TABLE mapa_cliente_fornecedor');
        $this->addSql('DROP TABLE mapa_estoque');
        $this->addSql('DROP TABLE mapa_movimento');
        $this->addSql('DROP TABLE mapa_parametro');
        $this->addSql('DROP TABLE mapa_produto');
    }
}
