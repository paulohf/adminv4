<?php

namespace App\Repository;

use App\Entity\MapaParametro;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MapaParametro|null find($id, $lockMode = null, $lockVersion = null)
 * @method MapaParametro|null findOneBy(array $criteria, array $orderBy = null)
 * @method MapaParametro[]    findAll()
 * @method MapaParametro[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MapaParametroRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MapaParametro::class);
    }

    // /**
    //  * @return MapaParametro[] Returns an array of MapaParametro objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MapaParametro
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
