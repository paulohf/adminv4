<?php

namespace App\Repository;

use App\Entity\MapaMovimento;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MapaMovimento|null find($id, $lockMode = null, $lockVersion = null)
 * @method MapaMovimento|null findOneBy(array $criteria, array $orderBy = null)
 * @method MapaMovimento[]    findAll()
 * @method MapaMovimento[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MapaMovimentoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MapaMovimento::class);
    }

    /**
     * @return MapaMovimento[] Returns an array of MapaMovimento objects
     */

    public function findByTipo($mes, $ano, $tipoProduto)
    {
        return $this->createQueryBuilder('m')
            ->join('m.produto', 'p')
            ->andWhere('m.ano = :ano')
            ->andWhere('m.mes = :mes')
            ->andWhere('p.tipoProd = :tipoProduto')
            ->setParameter('ano', $ano)
            ->setParameter('mes', $mes)
            ->setParameter('tipoProduto', $tipoProduto)
            ->orderBy('m.id', 'ASC')
            ->select('m')
            ->getQuery()
            ->getResult();
    }


    /*
    public function findOneBySomeField($value): ?MapaMovimento
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
