<?php

namespace App\Repository;

use App\Entity\MapaClienteFornecedorEnd;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MapaClienteFornecedorEnd|null find($id, $lockMode = null, $lockVersion = null)
 * @method MapaClienteFornecedorEnd|null findOneBy(array $criteria, array $orderBy = null)
 * @method MapaClienteFornecedorEnd[]    findAll()
 * @method MapaClienteFornecedorEnd[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MapaClienteFornecedorEndRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MapaClienteFornecedorEnd::class);
    }

    // /**
    //  * @return MapaClienteFornecedorEnd[] Returns an array of MapaClienteFornecedorEnd objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MapaClienteFornecedorEnd
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
