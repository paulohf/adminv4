<?php

namespace App\Form;

use App\Entity\MapaCalibre;
use App\Entity\MapaProduto;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MapaProdutoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('codigo')
            ->add('descricao')
            ->add('fabricante')
            ->add('tipoProd', ChoiceType::class, array('choices' => array(
                'Arma' => 'arma',
                'Munição' => 'municao'
            )))
            ->add('calibre', EntityType::class, [
                'class' => MapaCalibre::class,
                'placeholder' => 'Selecione o tipo',
                'choice_label' => function ($calibre) {
                    return $calibre->getDescricao() . ' - [ ' . $calibre->getTipoCalibre() . ' ]';
                },
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MapaProduto::class,
        ]);
    }
}
