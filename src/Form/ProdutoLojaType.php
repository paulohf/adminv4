<?php
/**
 * Created by PhpStorm.
 * User: paulo
 * Date: 03/08/2019
 * Time: 09:43
 */

namespace App\Form;


use App\Entity\ProdutoVirtual;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProdutoLojaType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('codigo',TextType::class)
            ->add('descricao', TextType::class)
            ->add('saldo',NumberType::class)
            ->add('qtyInventario',NumberType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
     $resolver->setDefaults(array("data_class"=>ProdutoVirtual::class));
    }


}