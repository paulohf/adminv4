<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Fornecedor;

class FornecedorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nome',TextType::class, array( 'label' => 'Nome Fantasia'))
            ->add('endereco',TextType::class, array( 'label' => 'Endereço'))
            ->add('telefone1', TextType::class, array('required' => false, 'label' => 'Telefone 1'))
            ->add('telefone2',TextType::class,array('required' => false, 'label' => 'Telefone 2'))
            ->add('observacao', TextareaType::class, array('required' => false, 'label' => 'Observações'))
            ->add('salvar', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => Fornecedor::class,));
    }

    public function getName()
    {
        return 'app_bundle_fornecedor_type';
    }
}
