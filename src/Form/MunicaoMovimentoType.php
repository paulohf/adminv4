<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MunicaoMovimentoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('produto', TextType::class)
            ->add('cliente', TextType::class)
            ->add('mes', IntegerType::class)
            ->add('ano', IntegerType::class)
            ->add('qtde', IntegerType::class)
            ->add('registro', TextType::class)
            ->add('docFiscal', TextType::class)
        ;


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults();
    }

    public function getName()
    {
        return 'app_bundle_mapamovimento_type';
    }
}
