<?php

namespace App\Form;

use App\Entity\SATProduto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SATProdutoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idProduto', HiddenType::class)
            ->add('codigo', TextType::class)
            ->add('descricao', TextType::class)
            ->add('origem', IntegerType::class)
            ->add('ncm', TextType::class)
            ->add('pis', NumberType::class)
            ->add('cofins', NumberType::class)
            ->add('simplesCsosn', TextType::class)
            ->add('icms', TextType::class)
            ->add('tributacao', ChoiceType::class,[
                'choices'  => [
                    'Tributado' => "TR",
                    'Não Tributado' => "NN",
                    'Substituição' => "FF",
                    'Isento' => "II"
                ]])
        ;


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => SATProduto::class));
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_satproduto';
    }
}
