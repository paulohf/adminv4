<?php

namespace App\Form;

use App\Entity\Funcionario;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FuncionarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', HiddenType::class)
            ->add('nome', TextType::class)
            ->add('rg', TextType::class)
            ->add('cpf', TextType::class)
            ->add('obs', TextareaType::class);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array("data_class" => Funcionario::class));
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_funcionario_type';
    }
}
