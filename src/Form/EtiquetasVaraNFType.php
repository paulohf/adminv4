<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class EtiquetasVaraNFType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('etiquetas', CollectionType::class, [
                'label' => false,
                'entry_options' => array('label' => false),
                'entry_type' => EtiquetaVaraNFType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'prototype' => true,
                'attr' => array(
                    'class' => 'form_collection',
                ),
                'by_reference' => false]
        )
        ->add('Imprimir',SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getName()
    {
        return 'app_bundle_etiquetas_vara_nf_type';
    }
}
