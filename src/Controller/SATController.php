<?php

namespace App\Controller;

use App\Entity\SATProduto;
use App\Form\SATProdutosType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class SATController extends AbstractController
{
    /**
     * @Route("/SAT/parametros/{date}",name="SATParam")
     *
     */
    public function indexAction(Request $request, $date)
    {
        $host = 'localhost:C:\\Forti Up\\DADOS\\AOPESCADOR.FDB';
        $charset = 'UTF8';
        $username = 'SYSDBA';
        $password = 'masterkey';
        $satprodutos = array();
        date_default_timezone_set("America/Sao_Paulo");
        $date = new \DateTime($date);
        $dbh = ibase_connect($host, $username, $password, $charset);
        $stmt = '
        SELECT MAX(B.data) AS "DATA",C.ID_PRODUTO, MAX(C.codigo) AS "CODIGO", MAX(C.descricao) AS "DESCRICAO", 
        MAX(C.simples_orig) AS "SIMPLES_ORIG", max(c.ncm) as "NCM", max(c.pis_cst) as "PIS_CST", 
        max(c.cofins_cst) as "COFINS_CST", max(c.simples_csosn) as "SIMPLES_CSOSN", max(c.tipo_aliquota) as "TRIBUTACAO", 
        max(c.icms_ecf) as "ICMS_ECF"
        FROM itempedido A, PEDIDO B, PRODUTOS C
        WHERE 1=1
            and A.id_pedido = B.id_pedido
            AND A.id_produto = C.id_produto
            AND B.data = \'' . $date->format('m-d-y') . '\'
        GROUP BY C.ID_PRODUTO
        order by descricao';
//        AND B.data = \'' . date_format($data, 'm-d-Y') . '\'
        $sth = ibase_query($dbh, $stmt);
        $em = $this->getDoctrine()->getManager();
        while ($row = ibase_fetch_object($sth)) {
            $produto = new SATProduto();
            $produto->setIdProduto($row->ID_PRODUTO);
            $produto->setCodigo($row->CODIGO);
            $produto->setDescricao(utf8_encode($row->DESCRICAO));
            $produto->setOrigem($row->SIMPLES_ORIG);
            $produto->setNcm(($row->NCM));
            $produto->setPis($row->PIS_CST);
            $produto->setCofins($row->COFINS_CST);
            $produto->setSimplesCsosn($row->SIMPLES_CSOSN);
            $produto->setTributacao($row->TRIBUTACAO);
            $produto->setIcms(intval($row->ICMS_ECF));
            array_push($satprodutos, $produto);
        }
        $form = $this->createForm(SATProdutosType::class, array('satprodutos' => $satprodutos));
//        $form->get('Data')->setData($date);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $retorno = $form->getData();
                $produtosSubmited = $retorno['satprodutos'];
                foreach ($produtosSubmited as $key => $prod) {
                    $satproduto = new SATProduto();
                    $satproduto = $prod;
                    $stmt = "update produtos a set a.simples_orig = " . $satproduto->getOrigem() .
                        ", a.pis_cst = " . $satproduto->getPis() .
                        ", a.cofins_cst = " . $satproduto->getCofins() .
                        ", a.ncm = " . $satproduto->getNcm() .
                        ", a.simples_csosn =" . $satproduto->getSimplesCsosn() .
                        ", a.icms_ecf = " . intval($satproduto->getIcms()) .
                        " where a.ID_PRODUTO = " . $satproduto->getIdProduto();

                    ibase_query($dbh, $stmt);
                }
                $this->addFlash(
                    'notice',
                    'Atualizações salvas com sucesso! '
                );
            } else {
                $this->addFlash(
                    'error',
                    'Falha ao atualizar registros! ' . $form->getErrors(true)
                );
            }
        }
        ibase_free_result($sth);
        ibase_close($dbh);
        return $this->render('default/page_sat_param.twig', array('form' => $form->createView(), 'data' => $date));
    }

    /**
     * @Route("/SAT2/parametros",name="SAT2Param")
     */
    public function sat2ParamAction(Request $request)
    {
        $host = 'localhost:C:\\Forti Up\\DADOS\\AOPESCADOR.FDB';
        $charset = 'UTF8';
        $username = 'SYSDBA';
        $password = 'masterkey';
        $satprodutos = array();


        $dbh = ibase_connect($host, $username, $password, $charset);
        $stmt = '
       SELECT  C.ID_PRODUTO,
        C.codigo,
        C.descricao,
        C.simples_orig,
        c.ncm,
        c.pis_cst,
        c.cofins_cst,
        c.simples_csosn,
        c.tipo_aliquota AS "TRIBUTACAO",
        c.icms_ecf,
        C.icms

        FROM  PRODUTOS C
        WHERE 1=1
              AND (
              (C.icms_ecf IS NULL and c.tipo_aliquota = \'TR\')
              OR c.cofins_cst is NULL 
              or c.pis_cst is NULL 
              or c.tipo_aliquota is NULL 
              or c.simples_orig is null
              
              )
              
              ';
        $sth = ibase_query($dbh, $stmt);
        $em = $this->getDoctrine()->getManager();
        while ($row = ibase_fetch_object($sth)) {
            $produto = new SATProduto();
            $produto->setIdProduto($row->ID_PRODUTO);
            $produto->setCodigo($row->CODIGO);
            $produto->setDescricao(utf8_encode($row->DESCRICAO));
            $produto->setOrigem($row->SIMPLES_ORIG);
            $produto->setNcm(($row->NCM));
            $produto->setPis(49);
            $produto->setCofins(49);
            $produto->setSimplesCsosn(102);
            $produto->setTributacao($row->TRIBUTACAO);
            $produto->setIcms(intval($row->ICMS_ECF));
            array_push($satprodutos, $produto);
        }
        $form = $this->createForm(SATProdutosType::class, array('satprodutos' => $satprodutos));


        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $retorno = $form->getData();
                $produtosSubmited = $retorno['satprodutos'];
                foreach ($produtosSubmited as $key => $prod) {
                    $satproduto = new SATProduto();
                    $satproduto = $prod;
                    $stmt = "update produtos a set a.simples_orig = " . $satproduto->getOrigem() .
                        ", a.pis_cst = " . $satproduto->getPis() .
                        ", a.cofins_cst = " . $satproduto->getCofins() .
                        ", a.ncm = " . $satproduto->getNcm() .
                        ", a.simples_csosn =" . $satproduto->getSimplesCsosn() .
                        ", a.icms_ecf = " . intval($satproduto->getIcms()) .
                        ", a.tipo_aliquota = '" . $satproduto->getTributacao() . "'" .
                        " where a.ID_PRODUTO = " . $satproduto->getIdProduto();

                    ibase_query($dbh, $stmt);
                }
                $this->addFlash(
                    'notice',
                    'Atualizações salvas com sucesso! '
                );
            } else {
                $this->addFlash(
                    'error',
                    'Falha ao atualizar registros! ' . $form->getErrors(true)
                );
            }
        }
        ibase_free_result($sth);
        ibase_close($dbh);
        return $this->render('default/sat/page_sat2_param.twig', array('form' => $form->createView()));
    }
}



