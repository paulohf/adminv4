<?php

namespace App\Controller;

use App\Form\EtiquetasVaraNFType;
use App\Entity\Etiqueta;
use App\Entity\NotaFiscal;
use App\Form\EtiquetasType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;


class EtiquetaProdutoController extends AbstractController
{
    private $host = 'localhost:C:\\Forti Up\\DADOS\\AOPESCADOR.FDB';
    private $username = 'SYSDBA';
    private $password = 'masterkey';
    private $charset = 'ISO8859_1';
    private $client;

    public function __construct(HttpClientInterface $tinyClient)
    {
        $this->client = $tinyClient;
    }


    /**
     * @Route("/etiqueta_carabina",name="EtiquetaCarabina")
     */
    public function printCarabinaTag(Request $request)
    {

        $fakeData = array('custo' => '709', 'margem' => '60', 'txdesconto' => '2.18', 'txflexrotativo' => '1.29',
            'txdeb' => '1.58', 'txflexparcelado' => '1.09');
        $form = $this->createFormBuilder($fakeData)
            ->add('custo', NumberType::class, array('label' => 'Custo'))
            ->add('margem', NumberType::class, array('label' => 'Margem'))
            ->add('txdesconto', NumberType::class, array('label' => 'Tx Desconto'))
            ->add('txflexrotativo', NumberType::class, array('label' => 'Tx Flex Rotativo'))
            ->add('txdeb', NumberType::class, array('label' => 'Tx Cartão de Débito'))
            ->add('txflexparcelado', NumberType::class, array('label' => 'Tx Flex Parcelado'))
            ->add('descricao1', TextType::class, array('label' => 'Descrição 1'))
            ->add('descricao2', TextType::class, array('label' => 'Descrição 2'))
            ->add('dnh', NumberType::class, array('label' => 'Dinheiro', 'required' => false))
            ->add('deb', NumberType::class, array('label' => 'Débito', 'required' => false))
            ->add('cr1', NumberType::class, array('label' => 'Parcela 1º', 'required' => false))
            ->add('cr2', NumberType::class, array('label' => 'Parcela 2º', 'required' => false))
            ->add('cr3', NumberType::class, array('label' => 'Parcela 3º', 'required' => false))
            ->add('cr4', NumberType::class, array('label' => 'Parcela 4º', 'required' => false))
            ->add('cr5', NumberType::class, array('label' => 'Parcela 5º', 'required' => false))
            ->add('cr6', NumberType::class, array('label' => 'Parcela 6º', 'required' => false))
            ->add('crt1', NumberType::class, array('label' => 'Total Parcela 1º', 'required' => false))
            ->add('crt2', NumberType::class, array('label' => 'Total Parcela 2º', 'required' => false))
            ->add('crt3', NumberType::class, array('label' => 'Total Parcela 3º', 'required' => false))
            ->add('crt4', NumberType::class, array('label' => 'Total Parcela 4º', 'required' => false))
            ->add('crt5', NumberType::class, array('label' => 'Total Parcela 5º', 'required' => false))
            ->add('crt6', NumberType::class, array('label' => 'Total Parcela 6º', 'required' => false))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->printCarabinaTagZPL($form->getData());

            }
        }

        return $this->render('default/page_imprimir_etiqueta_carabina.twig', array('form' => $form->createView()));
    }


    /**
     * @Route("/etiqueta_produto",name="EtiquetaProduto")
     *
     */
    public function listaNotasFiscaisProdutoAction(Request $request)
    {
        $response = $this->client
            ->request('GET', '/api2/notas.fiscais.pesquisa.php', [
                'query' => [
                    'dataInial' => '01/07/2021',
                    'tipoNota' => 'E'
                ]
            ]);
        $statusCode = $response->getStatusCode();
        $content = $response->getContent();
        $content = $response->toArray();
        $notas = $content['retorno'];


        $dbh = ibase_connect($this->host, $this->username, $this->password, $this->charset);
        $stmt = 'select a.id_notafiscal, a.codigo, a.nome_vinculo, a.data
                  from notafiscal a
                  where a.tipo_notafiscal = \'E\'
                  and a.data > dateadd (-180 day to current_date)
                  order by a.data desc';
        $sth = ibase_query($dbh, $stmt);

        $resultado = array();
        while ($row = ibase_fetch_object($sth)) {
            $nota = new NotaFiscal();
            $nota->setIdNF($row->ID_NOTAFISCAL);
            $nota->setNota($row->CODIGO);
            $nota->setFornecedor(utf8_encode($row->NOME_VINCULO));
            $nota->setData($row->DATA);
            array_push($resultado, $nota);

        }
        if ($notas['status'] == 'OK') {
            foreach ($notas['notas_fiscais'] as $valor) {
                $nota = new NotaFiscal();
                $nota->setIdNF($valor['nota_fiscal']['id']);
                $nota->setNota($valor['nota_fiscal']['numero']);
                $nota->setFornecedor($valor['nota_fiscal']['nome']);
                $nota->setData($valor['nota_fiscal']['data_emissao']);
                array_push($resultado, $nota);
            }
        }
        ibase_free_result($sth);
        ibase_close($dbh);
        return $this->render('default/page_etiqueta_produto.twig', array('notas' => $resultado));

    }

    /**
     * @Route("/etiqueta_vara_nf",name="EtiquetaVaraNF")
     *
     */
    public function listaNotasFiscaisVaraAction(Request $request)
    {
        $response = $this->client
            ->request('GET', '/api2/notas.fiscais.pesquisa.php', [
                'query' => [
                    'tipoNota' => 'E'
                ]
            ]);
        $statusCode = $response->getStatusCode();
        $content = $response->toArray();
        $retorno = $content['retorno'];
        $notas = array();
        if ($retorno['status'] == 'OK') {
            foreach ($retorno['notas_fiscais'] as $item) {
                $nota = new NotaFiscal();
                $nota->setIdNF($item['nota_fiscal']['id']);
                $nota->setNota($item['nota_fiscal']['numero']);
                $nota->setFornecedor($item['nota_fiscal']['nome']);
                $nota->setData($item['nota_fiscal']['data_emissao']);
                array_push($notas, $nota);

            }
        }


        return $this->render('default/page_etiqueta_vara_nf.twig', array('notas' => $notas));

    }


    /**
     * @Route("/etiqueta_produto_avulsa", name="EtiquetaAvulsa")
     */

    public function imprimirEtiquetaProdutoAvulsaAction(Request $request)
    {
        $etiquetas = array();
        $etiqueta = new Etiqueta();
        array_push($etiquetas, $etiqueta);
        $form = $this->createForm(EtiquetasType::class, array('etiquetas' => $etiquetas));
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $retorno = $form->getData();
                $em = $this->getDoctrine()->getManager();
                foreach ($retorno as $produtos) {
                }
                $this->printProdTagZPL($produtos);
            } else {
                $this->addFlash(
                    'error',
                    'Falha ao imprimir registros! ' . $form->getErrors(true)
                );
            }
        }
        return $this->render('default/page_imprimir_etiqueta_produto_avulso.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/etiqueta_vara_avulsa", name="EtiquetaVaraAvulsa")
     */

    public
    function imprimirEtiquetaVaraAvulsaAction(Request $request)
    {
        $etiquetas = array();
        $etiqueta = new Etiqueta();
        array_push($etiquetas, $etiqueta);
        $form = $this->createForm(EtiquetasVaraNFType::class, array('etiquetas' => $etiquetas));

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $retorno = $form->getData();
            $em = $this->getDoctrine()->getManager();
            foreach ($retorno as $produtos) {

            }

            $this->printVaraTagZPL($produtos);


        }
        return $this->render('default/page_imprimir_etiqueta_vara_avulso.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/imprimir_etiqueta/{nota}", name="ImprimirEtiqueta")
     */

    public function imprimirEtiquetaProdutoNotaAction(Request $request, $nota)
    {
        $host = 'localhost:C:\\Forti Up\\DADOS\\AOPESCADOR.FDB';
        $charset = 'ISO8859_9';
        $username = 'SYSDBA';
        $password = 'masterkey';
        $etiquetas = array();

        $dbh = ibase_connect($host, $username, $password, $charset);
        $stmt = '
              select * from (
                  select b.id_produto, max(b.codigo) as codigo, max(b.descricao) as descricao, 
                  sum(a.quantidade) as quantidade, max(b.precovenda5) as preco, max(b.data_venc) as data
                  from item_notafiscal a, produtos b
                  where a.id_notafiscal = ' . $nota . '
                        and (b.status_exclusao = \'N\' or b.status_exclusao is null)  
                        and b.id_produto = a.id_produto
                        and b.descricao not like \'V %\'
                  group by b.id_produto)    
               order by descricao
                     ';
        $sth = ibase_query($dbh, $stmt);
        $row = ibase_fetch_object($sth);

        if ($row) {
            while ($row) {
                $etiqueta = new Etiqueta();
                $etiqueta->setIdProduto($row->ID_PRODUTO);
                $etiqueta->setCodigo($row->CODIGO);
                $etiqueta->setDescEtiqueta(utf8_encode($row->DESCRICAO));
                $etiqueta->setDescricao(utf8_encode($row->DESCRICAO));
                $etiqueta->setQtde($row->QUANTIDADE);
                if ($row->DATA != null) {
                    $etiqueta->setData(new \DateTime($row->DATA));
                }
                $etiqueta->setPreco($row->PRECO);
                array_push($etiquetas, $etiqueta);
            }

            $stmt = 'select a.nome_vinculo, a.data, a.codigo from notafiscal a where a.id_notafiscal = ' . $nota;
            $sth = ibase_query($dbh, $stmt);
            $row = ibase_fetch_object($sth);
            $dadosNota = array('numero' => $row->CODIGO, 'nome' => utf8_encode($row->NOME_VINCULO), 'data' => $row->DATA);
        } else {
            $response = $this->client
                ->request('GET', '/api2/nota.fiscal.obter.php', [
                    'query' => [
                        'id' => $nota
                    ]
                ]);
            $statusCode = $response->getStatusCode();
            $content = $response->toArray();
            $retorno = $content['retorno'];
            if ($retorno['status'] == 'OK') {
                $notaFiscal = $retorno['nota_fiscal'];
                foreach ($notaFiscal['itens'] as $itemArray) {
                    $item = $itemArray['item'];
                    $etiqueta = new Etiqueta();
                    $etiqueta->setIdProduto($item['id_produto']);
                    $etiqueta->setCodigo($item['codigo']);
                    $etiqueta->setDescEtiqueta($item['descricao']);
                    $etiqueta->setDescricao($item['descricao']);
                    $etiqueta->setQtde($item['quantidade']);
                    $etiqueta->setData(new \DateTime());
                    sleep(2);
                    $responseProd = $this->client
                        ->request('GET', '/api2/produto.obter.php', [
                            'query' => [
                                'id' => $item['id_produto']
                            ]
                        ]);
                    $responseProd = $responseProd->toArray();
                    $retornoProd = $responseProd['retorno'];
                    if ($retornoProd['status'] == 'OK') {
                        $produto = $retornoProd['produto'];
                        $etiqueta->setPreco($produto['preco']);
                        $etiqueta->setDescEtiqueta($produto['nome']);
                        $etiqueta->setDescricao($produto['nome']);
                        $etiqueta->setCodigo($produto['codigo']);

                    } else {
                        $etiqueta->setPreco(0);
                    }
                    array_push($etiquetas, $etiqueta);
                }
                $dadosNota = array(
                    'numero' => $notaFiscal['numero'],
                    'nome' => $notaFiscal['cliente']['nome'],
                    'data' => $notaFiscal['data_emissao']
                );

            }
        }
        $form = $this->createForm(EtiquetasType::class, array('etiquetas' => $etiquetas));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $retorno = $form->getData();
            foreach ($retorno as $produtos) {
                foreach ($produtos as $key => $prod) {
                    if ($prod->getDescEtiqueta() <> "") {

                    }
                }
            }

            $this->printProdTagZPL($produtos);

        }

        ibase_free_result($sth);
        ibase_close($dbh);

        return $this->render('default/page_imprimir_etiqueta_produto.twig'
            , array('form' => $form->createView(), 'nota' => $dadosNota));
    }

    /**
     * @Route("/imprimir_etiqueta_vara_nf/{nota}", name="ImprimirEtiquetaVaraNF")
     */

    public
    function imprimirEtiquetaVaraNotaAction(Request $request, $nota)
    {
        $response = $this->client
            ->request('GET', '/api2/nota.fiscal.obter.php', [
                'query' => [
                    'id' => $nota
                ]
            ]);
        $statusCode = $response->getStatusCode();
        $content = $response->toArray();
        $retorno = $content['retorno'];
        $etiquetas = array();
        if ($retorno['status'] == 'OK') {
            foreach ($retorno['nota_fiscal']['itens'] as $item) {
                if (str_contains(strtoupper($item['item']['descricao']), 'VARA')
                    or str_contains(strtoupper($item['item']['descricao']), 'V ')
                ) {
                    $etiqueta = new Etiqueta();
                    $etiqueta->setIdProduto($item['item']['id_produto']);

                    $responseProd = $this->client
                        ->request('GET', '/api2/produto.obter.php', [
                            'query' => [
                                'id' => $item['item']['id_produto']
                            ]
                        ]);
                    $responseProd = $responseProd->toArray();
                    $retornoProd = $responseProd['retorno'];
                    if ($retornoProd['status'] == 'OK') {
                        sleep(2);
                        $etiqueta->setCodigo($retornoProd['produto']['codigo']);
                        $etiqueta->setDescricao($retornoProd['produto']['nome']);
                        $etiqueta->setPreco($retornoProd['produto']['preco']);
                    }

                    $etiqueta->setQtde($item['item']['quantidade']);

                    array_push($etiquetas, $etiqueta);
                }
            }
        }

        $form = $this->createForm(EtiquetasVaraNFType::class, array('etiquetas' => $etiquetas));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $retorno = $form->getData();
            foreach ($retorno as $produtos) {
                foreach ($produtos as $key => $prod) {
//                  #TODO: GRAVAR OS PREÇOS NA TABELA DE PRODUTOS
                }
            }

            $this->printVaraTagZPL($produtos);

        }


        return $this->render('default/page_imprimir_etiqueta_vara_nf.twig', array('form' => $form->createView()));
    }

    function strToHexZpl($string)
    {
        $hex = '';
        for ($i = 0; $i < strlen($string); $i++) {
            $hex .= '\\' . dechex(ord($string[$i]));
        }
        return $hex;
    }

    function printProdTagZPL($produtos)
    {

        $em = $this->getDoctrine()->getManager();
        $parametro = $em->getRepository('App:Parametros')->findOneBy(array('id' => 1));

        $zpl_data = '';
        $zpl_aux = '';
        $zpl_start = '^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR2,2~SD' . $parametro->getPrinterDarkness() . '^JUS^LRN^CI0^XZ';


        $zpl_base = $this->getZplProdCode();

        $zpl_aux = $zpl_start . $zpl_base;
        $etiquetaOpen = 0; // 0-fechada 1-falta uma etiqueta 2-falta duas etiquetas

        foreach ($produtos as $key => $prod) {

            while ($prod->getQtde() > 0) {

                $codigo = $prod->getCodigo();
                $descricao1 = $this->strToHexZpl(substr($prod->getDescEtiqueta(), 0, 30));
                $descricao2 = $this->strToHexZpl(substr($prod->getDescEtiqueta(), 30, 30));
                $valor = number_format($prod->getPreco(), 2, ',', '.');

                if ($etiquetaOpen == 0) {
                    if ($prod->getQtde() >= 3) {
                        $zpl_aux = str_replace('codigo1', $codigo, $zpl_aux);
                        $zpl_aux = str_replace('codigo2', $codigo, $zpl_aux);
                        $zpl_aux = str_replace('codigo3', $codigo, $zpl_aux);
                        $zpl_aux = str_replace('descricao1', $descricao1, $zpl_aux);
                        $zpl_aux = str_replace('descricao2', $descricao1, $zpl_aux);
                        $zpl_aux = str_replace('descricao3', $descricao1, $zpl_aux);
                        $zpl_aux = str_replace('descricao_11', $descricao2, $zpl_aux);
                        $zpl_aux = str_replace('descricao_22', $descricao2, $zpl_aux);
                        $zpl_aux = str_replace('descricao_33', $descricao2, $zpl_aux);
                        $zpl_aux = str_replace('valor1', number_format($prod->getPreco(), 2, ',', '.'), $zpl_aux);
                        $zpl_aux = str_replace('valor2', number_format($prod->getPreco(), 2, ',', '.'), $zpl_aux);
                        $zpl_aux = str_replace('valor3', number_format($prod->getPreco(), 2, ',', '.'), $zpl_aux);
                        $zpl_data .= $zpl_aux;
                        $zpl_aux = $zpl_base;
                        $prod->setQtde($prod->getQtde() - 3);
                    }

                    if ($prod->getQtde() == 2) {
                        $zpl_aux = str_replace('codigo1', $codigo, $zpl_aux);
                        $zpl_aux = str_replace('codigo2', $codigo, $zpl_aux);
                        $zpl_aux = str_replace('descricao1', $descricao1, $zpl_aux);
                        $zpl_aux = str_replace('descricao2', $descricao1, $zpl_aux);
                        $zpl_aux = str_replace('descricao_11', $descricao2, $zpl_aux);
                        $zpl_aux = str_replace('descricao_22', $descricao2, $zpl_aux);
                        $zpl_aux = str_replace('valor1', number_format($prod->getPreco(), 2, ',', '.'), $zpl_aux);
                        $zpl_aux = str_replace('valor2', number_format($prod->getPreco(), 2, ',', '.'), $zpl_aux);
                        $prod->setQtde($prod->getQtde() - 2);
                        $etiquetaOpen = 1;
                    }

                    if ($prod->getQtde() == 1) {
                        $zpl_aux = str_replace('codigo1', $codigo, $zpl_aux);
                        $zpl_aux = str_replace('descricao1', $descricao1, $zpl_aux);
                        $zpl_aux = str_replace('descricao_11', $descricao2, $zpl_aux);
                        $zpl_aux = str_replace('valor1', number_format($prod->getPreco(), 2, ',', '.'), $zpl_aux);
                        $prod->setQtde($prod->getQtde() - 1);
                        $etiquetaOpen = 2;
                    }
                }

                if ($etiquetaOpen == 2) {
                    if ($prod->getQtde() >= 2) {
                        $zpl_aux = str_replace('codigo2', $codigo, $zpl_aux);
                        $zpl_aux = str_replace('codigo3', $codigo, $zpl_aux);
                        $zpl_aux = str_replace('descricao2', $descricao1, $zpl_aux);
                        $zpl_aux = str_replace('descricao3', $descricao1, $zpl_aux);
                        $zpl_aux = str_replace('descricao_22', $descricao2, $zpl_aux);
                        $zpl_aux = str_replace('descricao_33', $descricao2, $zpl_aux);
                        $zpl_aux = str_replace('valor2', number_format($prod->getPreco(), 2, ',', '.'), $zpl_aux);
                        $zpl_aux = str_replace('valor3', number_format($prod->getPreco(), 2, ',', '.'), $zpl_aux);
                        $zpl_data .= $zpl_aux;
                        $zpl_aux = $zpl_base;
                        $prod->setQtde($prod->getQtde() - 2);
                        $etiquetaOpen = 0;
                    }
                    if ($prod->getQtde() == 1) {
                        $zpl_aux = str_replace('codigo2', $codigo, $zpl_aux);
                        $zpl_aux = str_replace('descricao2', $descricao1, $zpl_aux);
                        $zpl_aux = str_replace('descricao_22', $descricao2, $zpl_aux);
                        $zpl_aux = str_replace('valor2', number_format($prod->getPreco(), 2, ',', '.'), $zpl_aux);
                        $prod->setQtde($prod->getQtde() - 1);
                        $etiquetaOpen = 1;
                    }
                }

                if ($etiquetaOpen == 1) {
                    if ($prod->getQtde() >= 1) {
                        $zpl_aux = str_replace('codigo3', $codigo, $zpl_aux);
                        $zpl_aux = str_replace('descricao3', $descricao1, $zpl_aux);
                        $zpl_aux = str_replace('descricao_33', $descricao2, $zpl_aux);
                        $zpl_aux = str_replace('valor3', number_format($prod->getPreco(), 2, ',', '.'), $zpl_aux);
                        $zpl_data .= $zpl_aux;
                        $zpl_aux = $zpl_base;
                        $prod->setQtde($prod->getQtde() - 1);
                        $etiquetaOpen = 0;
                    }
                }

            }

        }
        if ($etiquetaOpen > 0) {
            $zpl_data .= $zpl_aux;
        }


        $filename = "C:/Users/paulo/Documents/NetBeansProjects/PrinterService/dist/etiqueta.txt";
        if (file_exists($filename)) {
            unlink($filename);
        }
        $fp = fopen($filename, "a");
        fwrite($fp, $zpl_data);
        if (file_exists($filename)) {
            exec("java -jar C:/Users/paulo/Documents/NetBeansProjects/PrinterService/dist/PrinterService.jar C:/Users/paulo/Documents/NetBeansProjects/PrinterService/dist/etiqueta.txt");
        }
    }

    function printVaraTagZPL($produtos)
    {
        $zpl_data = '';
        $zpl_aux = '';
        $zpl_start = '^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR2,2~SD15^JUS^LRN^CI0^XZ';
        $base1 = '~DG000.GRF,02688,028,
,::::::::::::::::I02020J020202020L020202020H03FRFE,H01E0E07F801FF9C0E7FC07F3FF00180I01FDFDFDFDFDFDFC,I0E0E0FFE07FF9C1E7
FE0FFBFF80380I03FRFE,I0F0E1F370F879C1C7F70E180300380I03FNF7F77E,I060E3C0F1E071FFC4078E080780380I03FRFE,I071E38079C071DF
80038E0H0700380I03FFDFHFLDFE,I03DE78079C070E380038F0H0707F80I03FRFE,I07FC70079C070E70H0387C0FF1FF80I03FOF7FFE,020FFE700
79C070E72003A7E0FF3FF0H0203FRFE,I0F1C70071C070E70H0381F15F3C70J03FDFDFDJDFDFC,H01E1C780F1C0F0EE0H0780F00F3870J03FF9A305
3A3BFFE,H01E1C780F1E0E0FE0H0F00700D3870J03FFD5A5155B7FFE,I0E1C3F3E0F8E07E0E7E1AF20E3E70J03FFE5AD3D227FFE,I0HFC1FFC07FE0
7C0FFC1FC1FE1FF0J03DFD5A519B97FDE,I03FC0FF803FE0780FF81FE3FE0FE0J03FFE671E3267FFE,U0780W03FIF7FMFE,I020H020H020H020H020
H020H020H020H03FE126183224FFE,gU01FC104181204DFC,gU03FF9E4093224FFE,gU03FF9E4011224FFE,Q01F0J0F0U03FF9260A1224FFE,JF1FI
FH07FC00E7FE00FD0FF17E1FE003FF904081224FFE,JF1FIF01FHF80FIF80FF0FF1FE1FE003FF9840F1204FFE,JF1FIF03FHFC0FIF80FF0FF1FE1FE
003FF9E4091200FFE,JF1FIF27FHFC0FIFC2FF0FF1FE1FE203FF904283220FFE,JF1FIF07FHFC0FIFC0FF0FF1FE1FE003FD904081010DFC,JF1FIF0
7FHFE0FIFE0FF0FF1FE1FE003FIFDFMFE,HF7F1FIF0FF1FE0FF1FE0FF0FF1FE1FE003FIF7FMFE,H0HFI0HF0FF1FE0FE1FE0FF0FF1FE1FE003FIFBFM
FE,H0HFI0HF0FF0FE0FE1FE0FF0FF1FE1FE003DFDD5FHFC01FDE,H0HFI0HF0FF0FE0FE1FE0FF0FF1FE1FE003FMFE00FFE,H0HFI0HF0FF0FE0FE1FE0
FF0FF1FE1FE003FIF7FHFI0HFE,H0HFI0HF0FF0FE2FE1FE0FF0FF3FE1FE003FMFI07FE,H0HFI0HF0FF0FE0FE1FE0FF0FF1FE1FE001FDDCFDFD0H05F
C,H0HFI0HF0FF0FE0FE1FE0FF0FF1FE1FE003FLFE2003FE,H0HFI0HF0FF0FE0FE1FE0FF0FF1FE1FE003FHFDFHFC7003FE,H0HFH02FF0FF0FE0FF9FE
0FF0FF1FE1FE003FHFDFHFEF003FE,H0HF0FIF0FF0FE0FF9FE0FF0FF1FE1FE003DHDJFDF803FE,H0HF0FIF0FF0FE0FF9FE0FF0FF1FE1FE003FHFBFH
FBFE03FE,H0HF0FIF0FF0FE0FF9FE0FF0FF1FF1FE003FF7FIF7FF07FE,02FF0FIF2FF0FE0FF9FE2FF0FF1FF1FE203FHF7FFEFFE0FFE,H0HF0FIF0FF
0FE0FF9FE0FF0FF1FF1FE003DDF5FDHDFC0DFC,H0HF0FIF0FF0FE0H01FE0FF0FF1FF9FE003FKFBFFE0FFE,H0HFI0HF0FF0FE0H01FE0FF0FF3FF9FE0
03F7EFFCFHF80FFE,H0HFI0HF0FF0FE0FF1FE0FF0FF3FF9FE003FJF3FHF80FFE,H0HFI0HF0FF0FE0FF1FC0FF0FF7FFDFE003DFC61FHFDE0FDE,H0HF
I0HF0FF0FE0FF1FE0FF0FJFDFE003FOF1FFE,H0HFI0HF0FF0FE0FF1FE0FF0FHFEFF7E0037FQFE,H0HFI0HF0FF0FE2FF1FE0FF0FHFEFHFE003FRFE,H
0HF0FIF0FF0FE0FDFFE0FF0FHFC7FFE001DDFDFDFDFDFDFC,H0HF0FIF0FF0FE0FIFC0FF0FHFC7FFE003FRFE,H0HF0FIF0FF0FE07FHFC0FF0FHFC7FF
E0037FQFE,H0HF0FIF0FF0FE07FHFC0FF0FHF83FFE003FRFE,H0HF0FIF0FF0FE03FHF80FF0FHF01FFE003FRFE,H0HF0FIF0FF0FE01FHFH0HF0FFE01
FFE003FRFE,H07F0FIF0FF0FE0077C00FD0FFE00FFE003FRFE,020H020H020H020H0202020H0202020H0203FRFE,,:::::::::::::::::::::^XA
^MMT
^PW559
^LL0320
^LS0
^FT352,320^XG000.GRF,1,1^FS
^FT318,243^A0I,76,74^FH\^FDR$ ';
        $base2 = 'valoravista' . '^FS
^FT363,185^A0I,28,28^FH\^FD' . 'descricao1' . '^FS
^FT363,151^A0I,28,28^FH\^FD' . 'descricao2' . '^FS
^FT461,121^BQN,2,4
^FH\^FDLA,' . 'codigo' . '^FS
^FT548,180^A0I,31,33^FH\^FD' . 'codigo' . '^FS
^FT445,24^A0I,31,31^FH\^FD3x de R$ ' . 'pvalor3' . ' total de R$ ' . 'tvalor3' . '^FS
^FT444,71^A0I,31,31^FH\^FD2x de R$ ' . 'pvalor2' . ' total de R$ ' . 'tvalor2' . '^FS';
        $base3 = '^FT345,103^A0I,28,28^FH\^FDPARCELAMENTO^FS
^FO12,222^GB537,0,1^FS
^FO9,135^GB538,0,1^FS
^FO451,7^GB0,129,2^FS
^PQ1,0,1,Y^XZ
^XA^ID000.GRF^FS^XZ
';

        $zpl_code = $zpl_start;

        foreach ($produtos as $key => $prod) {

            while ($prod->getQtde() > 0) {


                $codigo = $prod->getCodigo();
                $descricao1 = substr($prod->getDescricao(), 0, 24);
                $descricao2 = substr($prod->getDescricao(), 24, 24);
                $valoravista = number_format($prod->getPreco(), 2, ',', '.');
                $valor2 = number_format($prod->getPreco(), 2, ',', '.');
                $valor3 = number_format($prod->getPreco(), 2, ',', '.');
                $pvalor2 = $prod->getPreco() / 2;
                $pvalor3 = $prod->getPreco() / 3;
                $zpl_aux = $base2;

                $zpl_aux = str_replace('codigo', $codigo, $zpl_aux);
                $zpl_aux = str_replace('descricao1', $descricao1, $zpl_aux);
                $zpl_aux = str_replace('descricao2', $descricao2, $zpl_aux);
                $zpl_aux = str_replace('valoravista', $valoravista, $zpl_aux);
                $zpl_aux = str_replace('tvalor2', $valor2, $zpl_aux);
                $zpl_aux = str_replace('tvalor3', $valor3, $zpl_aux);
                $zpl_aux = str_replace('pvalor3', number_format($pvalor3, 2, ',', '.'), $zpl_aux);
                $zpl_aux = str_replace('pvalor2', number_format($pvalor2, 2, ',', '.'), $zpl_aux);
                $zpl_code .= $base1;
                $zpl_code .= $zpl_aux;
                $zpl_code .= $base3;


                $prod->setQtde($prod->getQtde() - 1);

            }
        }

        $filename = "C:/Users/paulo/Documents/NetBeansProjects/PrinterService/dist/etiqueta.txt";
        if (file_exists($filename)) {
            unlink($filename);
        }
        $fp = fopen($filename, "a");
        fwrite($fp, $zpl_code);
        if (file_exists($filename)) {
            exec("java -jar C:/Users/paulo/Documents/NetBeansProjects/PrinterService/dist/PrinterService.jar C:/Users/paulo/Documents/NetBeansProjects/PrinterService/dist/etiqueta.txt");
        }
    }

    function printCarabinaTagZPL($carabina)
    {
        $zplCode = $this->getZplCarabina();
        $zplCode = str_replace('DESCRICAOCARABINA11', $carabina['descricao1'], $zplCode);
        $zplCode = str_replace('DESCRICAOCARABINA22', $carabina['descricao2'], $zplCode);
        $zplCode = str_replace('CR1', str_replace('.', ',', $carabina['cr1']), $zplCode);
        $zplCode = str_replace('CR2', str_replace('.', ',', $carabina['cr2']), $zplCode);
        $zplCode = str_replace('CR3', str_replace('.', ',', $carabina['cr3']), $zplCode);
        $zplCode = str_replace('CR4', str_replace('.', ',', $carabina['cr4']), $zplCode);
        $zplCode = str_replace('CR5', str_replace('.', ',', $carabina['cr5']), $zplCode);
        $zplCode = str_replace('CR6', str_replace('.', ',', $carabina['cr6']), $zplCode);

        $zplCode = str_replace('CRT1', $carabina['crt1'], $zplCode);
        $zplCode = str_replace('CRT2', $carabina['crt2'], $zplCode);
        $zplCode = str_replace('CRT3', $carabina['crt3'], $zplCode);
        $zplCode = str_replace('CRT4', $carabina['crt4'], $zplCode);
        $zplCode = str_replace('CRT5', $carabina['crt5'], $zplCode);
        $zplCode = str_replace('CRT6', $carabina['crt6'], $zplCode);

        $zplCode = str_replace('DEB', $carabina['deb'], $zplCode);
        $zplCode = str_replace('DNH', $carabina['dnh'], $zplCode);

        $filename = "C:/Users/paulo/Documents/NetBeansProjects/PrinterService/dist/carabina.txt";
        if (file_exists($filename)) {
            unlink($filename);
        }
        $fp = fopen($filename, "a");
        fwrite($fp, $zplCode);
        if (file_exists($filename)) {
            exec("java -jar C:/Users/paulo/Documents/NetBeansProjects/PrinterService/dist/PrinterService.jar C:/Users/paulo/Documents/NetBeansProjects/PrinterService/dist/carabina.txt");
        }

    }


    public
    function getZplProdCode()
    {
        return "~DG000.GRF,00768,012,
        ,::::::::3F03F1F03E078F1FF1E0,3F87F1F0FF078F1FF1E0,3FC7F1F1FF878F1FF1E0,3FCFF1F1FFC78F1FF1E0,3FEFF1F1E7C78F1E01E0,3FIF1F1E7C78F1E01E0,3FFEF1F3E7C78F1E01E0,3EFCF1F3E7C78F1E01E0,3EFCF1F3E0078F1FF1E0,3E7CF1F3E7C78F1FF1E0,:3E78F1F3E7C78F1FF1E0,3E78F1F3E3C78F1E01E0,::3E78F1F3C3C78F1E01E0,3E78F1F3E3C78F1E01E0,3E78F1F1F7C79F1E01E0,3E78F1F1FFC7FF1FF1FF,3E78F1F1FFC7FE1FF1FF,3E78F1F0FFC3FE1FF1FF,145051503C40F01K1,,::H078FCF0F871F03E1F,H06CC193F871BC7F9B80,H0H4C18300F18CC19980,H0HCE9C600F186C19980,H0FCF8E6019106C19F,H0C1807601930CC193,H0C1803709FB0CC311,H0C1FBE3FB1BF87F3180,H0C1F9C0E61BF01C3180,,::::::::::::::::::::~DG001.GRF,01024,016,
        ,::::::::M03F03F1F03E078F1FF1E,M03F87F1F0FF078F1FF1E,M03FC7F1F1FF878F1FF1E,M03FCFF1F1FFC78F1FF1E,M03FEFF1F1E7C78F1E01E,M03FIF1F1E7C78F1E01E,M03FFEF1F3E7C78F1E01E,M03EFCF1F3E7C78F1E01E,M03EFCF1F3E0078F1FF1E,M03E7CF1F3E7C78F1FF1E,:M03E78F1F3E7C78F1FF1E,M03E78F1F3E3C78F1E01E,::M03E78F1F3C3C78F1E01E,M03E78F1F3E3C78F1E01E,M03E78F1F1F7C79F1E01E,M03E78F1F1FFC7FF1FF1FF0,M03E78F1F1FFC7FE1FF1FF0,M03E78F1F0FFC3FE1FF1FF0,M0145051503C40F01K10,,::O078FCF0F871F03E1F0,O06CC193F871BC7F9B8,O0H4C18300F18CC1998,O0HCE9C600F186C1998,O0FCF8E6019106C19F0,O0C1807601930CC1930,O0C1803709FB0CC3110,O0C1FBE3FB1BF87F318,O0C1F9C0E61BF01C318,,::::::::::::::::::::~DG002.GRF,01024,016,
        ,::::::::L07E07E3E07C0F1E3FE3C0,L07F0FE3E1FE0F1E3FE3C0,L07F8FE3E3FF0F1E3FE3C0,L07F9FE3E3FF8F1E3FE3C0,L07FDFE3E3CF8F1E3C03C0,L07FHFE3E3CF8F1E3C03C0,L07FFDE3E7CF8F1E3C03C0,L07DF9E3E7CF8F1E3C03C0,L07DF9E3E7C00F1E3FE3C0,L07CF9E3E7CF8F1E3FE3C0,:L07CF1E3E7CF8F1E3FE3C0,L07CF1E3E7C78F1E3C03C0,::L07CF1E3E7878F1E3C03C0,L07CF1E3E7C78F1E3C03C0,L07CF1E3E3EF8F3E3C03C0,L07CF1E3E3FF8FFE3FE3FE,L07CF1E3E3FF8FFC3FE3FE,L07CF1E3E1FF87FC3FE3FE,L028A0A2A07881E02K2,,::N0F1F9E1F0E3E07C3E,N0D98327F0E378FF37,N089830601E31983H3,M0199D38C01E30D83H3,M01F9F1CC03220D833E,M018300EC0326198326,M0183006E13F6198622,M0183F7C7F637F0FE63,M0183F381CC37E03863,,::::::::::::::::::::^XA
        ^MMT
        ^PW831
        ^LL0176
        ^LS0
        ^FT15,192^XG000.GRF,1,1^FS
        ^FT256,192^XG001.GRF,1,1^FS
        ^FT544,192^XG002.GRF,1,1^FS
        ^FT15,38^A0N,37,57^FH\^FDR$  valor1^FS
        ^FT15,63^A0N,17,16^FH\^FDdescricao1^FS
        ^FT15,84^A0N,17,16^FH\^FDdescricao_11^FS
        ^FT175,183^BQN,2,4
        ^FH\^FDMA,codigo1^FS
        ^FT15,125^A0N,34,31^FH\^FDcodigo1^FS
        ^FO3,43^GB259,0,4^FS
        ^FT90,168^A0N,15,12^FH\^FD(19)3342-7701^FS
        ^FT90,150^A0N,15,12^FH\^FD(19)3342-7702^FS
        ^FT288,38^A0N,37,57^FH\^FDR$  valor2^FS
        ^FT287,63^A0N,17,16^FH\^FDdescricao2^FS
        ^FT287,84^A0N,17,16^FH\^FDdescricao_22^FS
        ^FT444,183^BQN,2,4
        ^FH\^FDMA,codigo2^FS
        ^FT284,125^A0N,34,31^FH\^FDcodigo2^FS
        ^FO287,43^GB259,0,4^FS
        ^FT364,168^A0N,15,12^FH\^FD(19)3342-7701^FS
        ^FT364,150^A0N,15,12^FH\^FD(19)3342-7702^FS
        ^FT571,38^A0N,37,57^FH\^FDR$  valor3^FS
        ^FT570,63^A0N,17,16^FH\^FDdescricao3^FS
        ^FT570,84^A0N,17,16^FH\^FDdescricao_33^FS
        ^FT727,183^BQN,2,4
        ^FH\^FDMA,codigo3^FS
        ^FT567,125^A0N,34,31^FH\^FDcodigo3^FS
        ^FO570,43^GB259,0,4^FS
        ^FT647,168^A0N,15,12^FH\^FD(19)3342-7701^FS
        ^FT647,150^A0N,15,12^FH\^FD(19)3342-7702^FS
        ^PQ1,0,1,Y^XZ
        ^XA^ID000.GRF^FS^XZ
        ^XA^ID001.GRF^FS^XZ
        ^XA^ID002.GRF^FS^XZ";
    }

    public
    function getZplCarabina()
    {
        $em = $this->getDoctrine()->getManager();
        $parametro = $em->getRepository('App:Parametros')->findOneBy(array('id' => 1));
        return '^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR2,2~SD15^JUS^LRN^CI0^XZ
~DG000.GRF,02688,028,
,:::::Q020020020g03FSFE0K0F0781FE007FF3C071FE01FCFHFH0580I03FDFHFDFHFDFHFDE0K0F0783FF80FHF1C0E3FF83FEFFE00780I03FSFE0K070707FFC1FF71C1E7FF87DEFFE0070J03FPFI7E0J027870F83E3E071FFC607C72200E027002003FSFE0K01871E01E3C0F1DFC001C70I0E0070J03DFDFDFDLDFC0K03C71E01E380F1FB8001C78001E00F0J03FSFE0K01F71C00E780F1E78001E7C01FE1FF0J03FOFH7HFE0K03FF3C00E780E1E70H03E3F03EE7FF0J03FSFE0K07FF3C01C780E0E70H01C0F83FE78F0J03FHFDFDFDIDIFE0K0F8F3C01E780E0EE0H03C07C01CF0E0J03FSFE0K0F0E1C01C38060EE0H01C03C01CF0E0J03FFE504506077FFE0H0200F2E1E23C3E1E0FE0027A83C01EF2E20H023FHF46B67AF77FFE0K070E0F9F81D1E0FC061F0C1C01C78E0J03DFC0C945040FDFC0K07FE0FHFH0HFE0780FFE0FF8FFC7FE0J03FFE80B4E16AFHFE0K03FE03FE007FC0780FFC07F0FFC3FE0J03FHF31C4C404FHFE0P020L0780080H080Q03FSFE0gY03FD001840C448FDE0gY03FF0010204H48FFE0gY03FFC712264H48FFE0J020H020H020H020H020H020H020H020H02003FFC732266448FFE0gY03DFC510064H48DFC0H0JF87FHFC00BF801E3FE003F83FC3FE1FE003FFC412H2I48FFE0H0JF87FHFC03FFE01F7FF807F83FC3FE1FE003FFC412H2I48FFE0H0JF87FHFC07FHF81FIFC07F83FC3FE1FE003FFC712H24600FFE0H0JF87FHFC0FIF81FIFC07F83FC3FE1FE003FFC510204400FFE0H0JF87FHFC0FIFC1FIFE07F83FC3FE1FE003FFC412204420FFE0H0JF87FHFC1FIFC1FIFE07F83FC3FE1FE003FFC412304430FFE0H0JFA7FHFC1FIFE3FJF07F83FE3FE3FE023FSFE0H0HF7F87F7FC1FC3FC1FF1FF07F83FC3FE1FE003DIDCFDFDFDFDFC0J07F8007FC1FE3FE1FF0FF07F83FC3FE1FE003FIFEFNFE0J07F8007FC1FE1FE1FE0FF07F83FC3FE1FE003FIF5FIFC01FFE0J07F8007FC1FE3FE1FE0FF07F83FC3FE1FE003FIFDFIFC03FFE0J07F8007FC3FE1FE1FE0FF07F83FC3FE1FE003FDJDHFE0H0FDE0J07F8007FC3FE1FE1FE0FF07F83FC3FE1FE003FIFBFHFE0H0HFE0J07F8007FC3FE1FE1FE0FF07F83FC3FE1FE003FIFBFHFC0H07FE0J07F8027FC3FE1FE1FE0FF27F83FE3FE3FE003FMFE0H07FE0J07F8007FC3FE1FE1FE0FF07F83FC3FE1FE003DHDF5FDF84005FC0J07F8007FC3FE1FE1FE0FF07F83FC3FE1FE003FMF9E003FE0J07F87FHFC3FE1FE1FF8FF07F83FC3FE1FE003FHFEFIF1E003FE0J07F87FHFC3FE3FE1FF8FF07F83FC3FE1FE003FHFEFIF7F803FE0J07F87FHFC3FE1FE1FF8FF07F83FC3FE1FE003DHDJFC7FC03FE0J07F87FHFC3FE1FE1FF8FF07F83FC3FE1FE003FHFDFHFCFFE0FFE0J07F87FHFC3FE1FE1FF8FF07F83FC3FE1FE003FHFDFHF9FFC0FFE0H0207FA7FHFC3FE1FE3FF8FF07F83FE3FE3FE023FHFBFHF3FFC1FFE0J07F87FHFC3FE1FE0I0HF07F83FC7FF1FE003DHDBDFCFDFC1DFC0J07F8003FC3FE1FE0I0HF07F83FC7FF1FE003FKF9FHF80FFE0J07F8007FC3FE1FE1FF0FF07F83FCFHF1FE003FHF7FC7FHFH0HFE0J07F8007FC3FE3FE1FF0FF07F83FCFHF9FE003FHF7E3FIFC2FFE0J07F8007FC3FE1FE1FF0FF07F83FDFHFDFE003DDFLFDE1FDE0J07F8007FC3FE1FE1FF0FF07F83FHFDFHFE003FSFE0J07F8007FC3FE1FE0FF0FF07F83FHFDFHFE003F7FQFE0J07F87FHFC3FE1FE0FF1FF27F83FHFDFHFE003FSFE0J07F87FHFC3FE1FE0FJF07F83FHF8FHFE003DFDFDFDFDFDFDFC0J07F87FHFC3FE1FE0FIFE07F83FHF8FHFE003FSFE0J07F87FHFC3FE1FE07FHFE07F83FHF07FFE003FSFE0J07F87FHFC3FE3FE07FHFE07F83FHF03FFE003FSFE0J07F87FHFC3FE1FE03FHFC07F83FFE03FFE003FSFE0J07F87FHFC3FE1FE01FHF807F83FFC01FFE003FSFE0J07F87FHFC3FE1FE005FE007F83FF800FFE003FSFE0H020H020H020H020H0202020H0202020H0202023FSFE0,::::::::::::::::::::::::::::^XA
^MMT
^PW559
^LL0309
^LS0
^FT0,320^XG000.GRF,1,1^FS
^FT472,21^A0R,28,26^FB265,1,0,C^FH\^FDDESCRICAOCARABINA22^FS
^FT513,21^A0R,28,26^FB265,1,0,C^FH\^FDDESCRICAOCARABINA11^FS
^FO445,21^GB0,275,2^FS
^FT419,33^A0I,23,24^FH\^FD6\A7^FS
^FT419,66^A0I,23,24^FH\^FD5\A7^FS
^FT419,200^A0I,23,24^FH\^FD1\A7^FS
^FT419,166^A0I,23,24^FH\^FD2\A7^FS
^FT419,133^A0I,23,24^FH\^FD3\A7^FS
^FT419,99^A0I,23,24^FH\^FD4\A7^FS
^FT444,233^A0I,23,24^FH\^FDD\82bito^FS
^FT340,67^A0I,23,24^FH\^FDR$ CR5^FS
^FT340,99^A0I,23,24^FH\^FDR$ CR4^FS
^FT340,133^A0I,23,24^FH\^FDR$ CR3^FS
^FT340,166^A0I,23,24^FH\^FDR$ CR2^FS
^FT340,199^A0I,23,24^FH\^FDR$ CR1^FS
^FT193,200^A0I,23,24^FH\^FDR$ CRT1^FS
^FT193,166^A0I,23,24^FH\^FDR$ CRT2^FS
^FT193,132^A0I,23,24^FH\^FDR$ CRT3^FS
^FT193,99^A0I,23,24^FH\^FDR$ CRT4^FS
^FT193,66^A0I,23,24^FH\^FDR$ CRT5^FS
^FT193,31^A0I,23,24^FH\^FDR$ CRT6^FS
^FT340,32^A0I,23,24^FH\^FDR$ CR6^FS
^FT338,236^A0I,23,24^FH\^FDR$ DEB^FS
^FT338,272^A0I,23,24^FH\^FDR$ DNH^FS
^FT444,272^A0I,23,24^FH\^FDDinheiro^FS
^FO223,22^GB0,274,1^FS
^FO357,23^GB0,273,2^FS
^FO10,22^GB434,0,2^FS
^FO9,57^GB434,0,2^FS
^FO9,89^GB434,0,3^FS
^FO10,123^GB435,0,2^FS
^FO10,157^GB434,0,2^FS
^FO10,190^GB435,0,2^FS
^FO10,223^GB434,0,2^FS
^FO223,292^GB220,0,3^FS
^FO224,263^GB221,0,3^FS
^PQ1,0,1,Y^XZ
^XA^ID000.GRF^FS^XZ
';
    }

}

