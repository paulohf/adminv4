<?php

namespace App\Controller;

use App\Entity\MapaClienteFornecedor;
use App\Entity\MapaProduto;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Produto;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class APIController extends AbstractController
{

    private $host = 'localhost:C:\\Forti Up\\DADOS\\AOPESCADOR.FDB';
    private $username = 'SYSDBA';
    private $password = 'masterkey';
    private $charset = 'ISO8859_9';

    public function __construct(HttpClientInterface $tinyClient)
    {
        $this->client = $tinyClient;
    }

    /**
     * @Route("/request/tiny/produto", name="request_tiny_produto",methods={"POST"})
     */
    public function getProdutoEtiquetaTiny(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $produtoRequest = "";
            $produtoRequest = $request->request->get('codigo');
            if ($produtoRequest <> "") {

                $response = $this->client
                    ->request('GET', '/api2/produtos.pesquisa.php', [
                        'query' => [
                            'pesquisa' => $produtoRequest,
                        ]
                    ]);
                $respListaProd = $response->toArray();
                if ($respListaProd['retorno']['status'] == 'OK') {
                    foreach ($respListaProd['retorno']['produtos'] as $produtos){
                        if($produtos['produto']['codigo']==$produtoRequest){
                            $id = $produtos['produto']['id'];
                        }
                    }
                    $response = $this->client
                        ->request('GET', '/api2/produto.obter.php', [
                            'query' => [
                                'id' => $id
                            ]
                        ]);
                    $produto = $response->toArray();
                    if ($produto['retorno']['status'] == 'OK') {
                        return new JsonResponse(array(
                            'codigo' => $produto['retorno']['produto']['codigo'],
                            'descricao' => $produto['retorno']['produto']['nome'],
                            'preco' => $produto['retorno']['produto']['preco'],
                            'descEtiqueta' => $produto['retorno']['produto']['nome'],
                            'idproduto' => $produto['retorno']['produto']['id'],
                            'avista' => $produto['retorno']['produto']['preco'],
                            'parc2' => $produto['retorno']['produto']['preco'],
                            'parc3' => $produto['retorno']['produto']['preco'],

                        ));
                    }
                }
                return new Response('Não foram encontrados registros para a consulta', 404);
            }
        }
        return new Response('This is not ajax!', 400);
    }

    /**
     * @Route("/etiqueta/produto/ajax", name="_etiqueta_produto_request",methods={"POST"})
     */
    public function getProdutoEtiqueta(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $data = "";
            $data = $request->request->get('codigo');
            if ($data <> "") {
                $dbh = ibase_connect($this->host, $this->username, $this->password, $this->charset);
                $stmt = 'select a.descricao, a.precovenda5, a.id_produto, a.precovenda4, a.precovenda, a.data_venc
                  from produtos a
                  where 1=1
                  and (a.status_exclusao = \'N\' or a.status_exclusao is null)
                  
                  and a.codigo =\'' . $data . '\'';
                $sth = ibase_query($dbh, $stmt);
                $em = $this->getDoctrine()->getManager();
                $descricao = "";
                $preco = "";
                $idproduto = "";
                $descEtiqueta = "";
                $preco1 = "";
                while ($row = ibase_fetch_object($sth)) {
                    $descricao = utf8_encode($row->DESCRICAO);
                    $preco = $row->PRECOVENDA5;
                    $idproduto = $row->ID_PRODUTO;
                    $preco1 = $row->PRECOVENDA;
                    $parc2x = $row->PRECOVENDA4;
                    $parc3x = $preco;
                    $dataUpdate = $row->DATA_VENC;
                }
                $descEtiqueta = $descricao;
            }
            ibase_free_result($sth);
            ibase_close($dbh);
            return new JsonResponse(array('descricao' => $descricao, 'preco' => $preco, 'descEtiqueta' => $descEtiqueta,
                'idproduto' => $idproduto, 'avista' => $preco1, 'parc2' => $parc2x, 'parc3' => $parc3x, 'dataVenc' => $dataUpdate));

        }
        return new Response('This is not ajax!', 400);
    }


    /**
     * @Route("/json/produto/descricao", name="_json_produto_descricao")
     *
     */
    public function getDescricaoProdutojson(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $data = "";
            $data = $request->request->get('term');

            $retorno = array();
            if ($data <> "") {
                $dbh = ibase_connect($this->host, $this->username, $this->password, $this->charset);
                $stmt = 'select a.codigo, a.descricao
                  from produtos a
                  where 1=1
                  and (a.status_exclusao = \'N\' or a.status_exclusao is null)
                  and UPPER(a.descricao) like UPPER(\'%' . $data . '%\') order by a.descricao';
                $sth = ibase_query($dbh, $stmt);
                while ($row = ibase_fetch_object($sth)) {
                    array_push($retorno, array('value' => $row->CODIGO, 'label' => (utf8_encode($row->DESCRICAO))));

                }
            }

            ibase_free_result($sth);
            ibase_close($dbh);
            return new JsonResponse($retorno);

        }
        return new Response('This is not ajax!', 400);
    }

    /**
     * @Route("/request/tiny/produto/descricao", name="tiny_produto_descricao")
     *
     */
    public function getDescricaoProdutoTiny(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $descRequest = $request->request->get('term');
            $response = $this->client
                ->request('GET', '/api2/produtos.pesquisa.php', [
                    'query' => [
                        'pesquisa' => $descRequest,
                    ]
                ]);
            $content = $response->toArray();
            $retorno = $content['retorno'];
            if ($retorno['status'] == 'OK') {
                $jsonProdutos = array();
                foreach ($retorno['produtos'] as $produto) {
                    array_push($jsonProdutos, array('value' => $produto['produto']['codigo'],
                        'label' => $produto['produto']['nome']));
                }
                return new JsonResponse($jsonProdutos);
            }
            return new Response('Não foram encontrados registros para a consulta', 404);
        }
        return new Response('This is not ajax!', 400);
    }

    /**
     * @Route("/request/mapa/cliente", name="request_mapa_cliente")
     *
     */
    function getMapaCliente(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $data = '%' . strtoupper($request->query->get('nome')) . '%';
            $repository = $this->getDoctrine()->getRepository(MapaClienteFornecedor::class);
            $clientes = $repository->findByName($data);
//            $clientes  =$repository->findAll();


            if ($clientes) {
                $clientesArray = array();
                foreach ($clientes as $cli) {
                    array_push($clientesArray, array(
                        "value" => $cli->getId(),
                        "label" => $cli->getNome(),
                    ));
                }

                return new JsonResponse(($clientesArray));
            }
            return new Response('Record not found', 404);
        }
        return new Response('This is not XmlHttpRequest', 400);
    }

    /**
     * @Route("/request/mapa/produto", name="request_mapa_produto")
     *
     */

    public function getProduto(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $data = $request->query->get('codigo');
            $repository = $this->getDoctrine()->getRepository(MapaProduto::class);
            $produto = $repository->findOneBy([
                'codigo' => $data
            ]);
            if ($produto) {
                $produtoArray = array();
                array_push($produtoArray, array(
                    "value" => $produto->getId(),
                    "label" => $produto->getDescricao(),
                ));
                return new JsonResponse(($produtoArray));
            }
            return new Response('Record not found', 404);
        }
        return new Response('This is not XmlHttpRequest', 400);
    }

}
