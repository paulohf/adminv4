<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ParametrosController extends AbstractController
{
    /**
     * @Route("/parametros",name="parametros")
     */
    public function parametrosAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $parametros = $em->getRepository('App:Parametros')->find(1);

        $form = $this->createFormBuilder($parametros)
            ->add("printer_darkness", IntegerType::class, array('label' => 'Cinza'))
            ->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted()){
            if($form->isValid()){
                $parametros=$form->getData();
                $em->persist($parametros);
                $em->flush();
                $this->addFlash(
                    'notice',
                    'Atualizações salvas com sucesso! '
                );

            }else{
                $this->addFlash(
                    'error',
                    'Falha ao atualizar registros! ' . $form->getErrors(true)
                );
            }
        }
        return $this->render('default/parametros/page_parametros.twig', array("form" => $form->createView()));
    }
}
