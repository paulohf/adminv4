<?php

namespace App\Controller\Mapa;

use App\Entity\MapaCalibre;
use App\Entity\MapaClienteFornecedor;
use App\Entity\MapaClienteFornecedorEnd;
use App\Entity\MapaEstoque;
use App\Entity\MapaMovimento;
use App\Entity\MapaMovimentoArmas;
use App\Entity\MapaParametro;
use App\Entity\MapaProduto;
use App\Entity\ParametrosMapa;
use App\Form\MunicaoMovimentoType;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\HttpClient\HttpClientInterface;


class MapaMovimentosController extends AbstractController
{

    private $caracteresEspeciais = array('-', '.', ',');
    private $host = 'localhost:C:\\Forti Up\\DADOS\\AOPESCADOR.FDB';
    private $charset = 'ISO8859_9';
    private $username = 'SYSDBA';
    private $password = 'masterkey';

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @Route("/mapa/movimentos", name="mapa_movimentos")
     */
    public function index(Request $request)
    {
        $repParametro = $this->getDoctrine()->getRepository(MapaParametro::class);
        $parametro = $repParametro->find(1);
        $this->findCreateAmmoMov();
        return $this->render('default/mapa_eb/mapa_movimentos.twig', [
            'controller_name' => 'MapaMovimentosController',
            'parametro' => $parametro


        ]);
    }

    /**
     * @Route ("/mapa/movimento/armas/mes", name="mapa_movimento_armas_mes")
     */
    public function mapaMovimentoArmasMes()
    {
        $parametrosMapa = $this->getDoctrine()->getRepository(ParametrosMapa::class);
        $paramentros = $parametrosMapa->find(1);

        /*codigo auxiliar para carregar armas na tabela de produtos*/
//        $produtos = $this->getDoctrine()->getRepository(MapaProduto::class);
//        $repCalibre = $this->getDoctrine()->getRepository(MapaCalibre::class);
//        $lines = file('c:\temp\armas.txt');
//        $entityManager = $this->getDoctrine()->getManager();
//        foreach ($lines as $line_num => $line) {
//            $produtoImportado = explode(';', $line);
//            $produto = $produtos->findBy(['codigo' => $produtoImportado[0]]);
//            if (!$produto) {
//                $novoProduto = new MapaProduto();
//                $novoProduto->setCodigo($produtoImportado[0]);
//                $novoProduto->setDescricao($produtoImportado[1]);
//                $calibre = $repCalibre->find($produtoImportado[2]);
//                if ($calibre) {
//                    $novoProduto->setCalibre($calibre);
//                }
//                $novoProduto->setTipoProd('arma');
//                $entityManager->persist($novoProduto);
//                $entityManager->flush();
//            }
//
//        }
        $this->findCreateGunMov();
        return $this->render('default/mapa_eb/mapa_movimentos_armas.twig', [
            "parametro" => $paramentros

        ]);
    }

    /**
     * @Route ("/mapa/processo/venda/armas", name="mapa_processo_venda_armas")
     */
    public function processoVendaArma()
    {
        $repParametro = $this->getDoctrine()->getRepository(ParametrosMapa::class);
        $parametro = $repParametro->find(1);

        return $this->render('default/mapa_eb/mapa_movimentos_armas.twig', [
            'controller_name' => 'MapaMovimentosController',
            'parametro' => $parametro


        ]);
    }

    /**
     * @Route ("/mapa/processo/venda/municaoV2", name="mapa_processo_venda_municaoV2")
     */
    public function findCreateAmmoMovSige()
    {
        $response = $this->client->request(
            'GET',
            'http://api.sigecloud.com.br/request/Pedidos/Pesquisar', [
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization-Token' => '0ca586c541db2f08065c29051abe15c48fa6f7becd32fc9d78e689ef5c17621c0862c9ca14344e169f3b9342010f85ede7d3c24fec10ed740db949c740a7fbabbb2973410bdc69b480f88889a7097c79b88ee132e48862039e1c8be331f295f661884fcdf77f3fb67ea5fd4f3c3a52039f5b4dd7f1f97bf3d42d85381bff5b63',
                    'User' => 'paulo@miguelpescador.com.br',
                    'App' => 'API'
                ],
                'query' => [
                    'dataInicial' => '01/05/2021',
                    'dataFinal' => '31/05/2021',
                    'filtrarPor' => 'DataFaturamentoPedido',
                    'status' => 'Pedido Faturado'
                ],
            ]
        );
        $content = $response->toArray();
        var_dump($content);

    }

    public function findCreateAmmoMov()
    {
        $host = 'localhost:C:\\Forti Up\\DADOS\\AOPESCADOR.FDB';
        $charset = 'ISO8859_9';
        $username = 'SYSDBA';
        $password = 'masterkey';

        $repParametro = $this->getDoctrine()->getRepository(MapaParametro::class);
        $repProdutos = $this->getDoctrine()->getRepository(MapaProduto::class);
        $repMovimento = $this->getDoctrine()->getRepository(MapaMovimento::class);
        $repClienteFornecedor = $this->getDoctrine()->getRepository(MapaClienteFornecedor::class);

        $entityManager = $this->getDoctrine()->getManager();

        $produtos = $repProdutos->findBy([
            'tipoProd' => 'municao'
        ]);

        $parametro = $repParametro->find(1);

        $dbh = ibase_connect($host, $username, $password, $charset);

        /* CRIA STRING COM PRODUTOS CONTROLADOS*/
        $csvProdutos = "";
        foreach ($produtos as $produto) {
            if (strlen($csvProdutos) == 0)
                $csvProdutos = "'" . $produto->getCodigo() . "'";
            else {
                $csvProdutos = $csvProdutos . ',' . "'" . $produto->getCodigo() . "'";
            }
        }

        /*PEGA OS PARAMENTROS DO SISTEMA PARA O MES EM ABERTO*/
        $aDate = $parametro->getAnoAberto() . '-' . $parametro->getMesAberto() . '-01';
        $dataInicial = $parametro->getMesAberto() . '-01-' . $parametro->getAnoAberto();
        $dataFinal = date("m-t-Y", strtotime($aDate));
        $csvProdutos = "";
        foreach ($produtos as $produto) {
            if (strlen($csvProdutos) == 0)
                $csvProdutos = "'" . $produto->getCodigo() . "'";
            else {
                $csvProdutos = $csvProdutos . ',' . "'" . $produto->getCodigo() . "'";
            }
        }

        $sqlSelect =
            "select "
            . " a.data, "
            . " c.CODIGO, "
            . " c.DESCRICAO, "
            . " d.FONE_CLIENTE as fone,"
            . " b.QUANTIDADE, "
            . " d.MENSAGEM1, "
            . " d.NOME_CLIENTE, "
            . " d.CNPJ_CLIENTE, "
            . " B.id_itemcupom_vend as id_item, "
            . " a.codigo_cupom as docfiscal,"
            . " 'S' as tipo"
            . " from "
            . " CUPOM_VEND a, "
            . " ITEMCUPOM_VEND b, "
            . " PRODUTOS c, "
            . " PEDIDO d "
            . " where "
            . "    a.ID_CUPOM_VEND = b.ID_CUPOM_VEND"
            . "    and b.ID_PRODUTO = c.ID_PRODUTO"
            . " and d.ID_CUPOM = a.ID_CUPOM_VEND"
            . "    and c.codigo in(" . $csvProdutos . ")"
            . "    and a.data between  "
            . " '" . $dataInicial . "' "
            . " and "
            . " '" . $dataFinal . "' "
            . " union all"
            . " select  "
            . " max(A.DATA) as DATA,"
            . " d.codigo,"
            . " max(c.descricao_produto) as descricao,"
            . " max(a.fone_vinculo) as fone,"
            . " sum(c.quantidade) as quantidade,"
            . " '' as mensagem1, "
            . " max(a.nome_vinculo) as nome_cliente,"
            . " max(a.cnpj_vinculo) as cnpj_cliente,"
            . " max(c.ID_ITEMNOTAFISCAL) as id_item,"
            . "  max(A.codigo) as docfiscal, "
            . " 'E' as tipo"
            . " from  "
            . "  NOTAFISCAL A,  "
            . "  NATOP B,  "
            . "  item_notafiscal c , "
            . "  produtos d "
            . "where  "
            . "  1=1 "
            . "  and d.ID_PRODUTO = c.ID_PRODUTO "
            . "  and A.ID_NATOP = B.ID_NATOP "
            . "  and B.CODIGO in ('1.102', '2.102','1.202') "
            . "  and A.STATUS = 'S' "
            . "  and c.id_notafiscal = a.id_notafiscal"
            . "  and d.codigo in (" . $csvProdutos . ")"
            . "    and a.data between "
            . " '" . $dataInicial . "' "
            . " and "
            . " '" . $dataFinal . " '"
            . "  group by a.codigo, d.codigo";


        $query = ibase_query($dbh, $sqlSelect);

        $movimentos = array();
        while ($row = ibase_fetch_object($query)) {
            $movimento = $repMovimento->findBy(['docFiscalItem' => $row->ID_ITEM]);
            if (!$movimento) {
                $movimento = new MapaMovimento();
                $movimento->setAno($parametro->getAnoAberto());
                $movimento->setMes($parametro->getMesAberto());
                $movimento->setQtde($row->QUANTIDADE);
                $movimento->setDocFiscal($row->DOCFISCAL);
                $movimento->setDocFiscalItem($row->ID_ITEM);
                if (is_null($row->MENSAGEM1)) {
                    $movimento->setRegistro('');
                } else {
                    $movimento->setRegistro($row->MENSAGEM1);
                }
                $movimento->setTipoMov($row->TIPO);
                $produto = $repProdutos->findOneBy(['codigo' => $row->CODIGO]);
                if ($produto) {
                    $movimento->setProduto($produto);
                }
                $clienteFornecedor = $repClienteFornecedor
                    ->findOneBy(['cpf' => trim(str_replace($this->caracteresEspeciais, '', $row->CNPJ_CLIENTE))]);
                if (!$clienteFornecedor) {
                    $clienteFornecedor = new MapaClienteFornecedor();
                    $clienteFornecedor->setNome(utf8_encode($row->NOME_CLIENTE));
                    $clienteFornecedor->setCpf(trim(str_replace($this->caracteresEspeciais, '', $row->CNPJ_CLIENTE)));
                    $clienteFornecedor->setTelefone(trim(str_replace($this->caracteresEspeciais, '', $row->FONE)));
                }
                if (!$clienteFornecedor->getEndereco()) {
                    $endereco = $this
                        ->getEndereco(str_replace($this->caracteresEspeciais, '', $row->CNPJ_CLIENTE));
                    if ($endereco) {
                        $clienteFornecedor->setEndereco($endereco);
                    }
                }
                $movimento->setClienteFornecedor($clienteFornecedor);
                $movimento->setClienteFornecedor($clienteFornecedor);
                $entityManager->persist($movimento);
                $entityManager->flush();
            }
        }
        ibase_free_result($query);
        ibase_close($dbh);
        $parametro->setDataUltProcMov(\DateTime::createFromFormat('Y-m-d', date('Y-m-d')));
        $entityManager->persist($parametro);
        $entityManager->flush();
    }

    public function findCreateGunMov()
    {
        $host = 'localhost:C:\\Forti Up\\DADOS\\AOPESCADOR.FDB';
        $charset = 'ISO8859_9';
        $username = 'SYSDBA';
        $password = 'masterkey';

        $repParametro = $this->getDoctrine()->getRepository(ParametrosMapa::class);
        $repProdutos = $this->getDoctrine()->getRepository(MapaProduto::class);
        $repMovimento = $this->getDoctrine()->getRepository(MapaMovimentoArmas::class);
        $repClienteFornecedor = $this->getDoctrine()->getRepository(MapaClienteFornecedor::class);

        $entityManager = $this->getDoctrine()->getManager();

        $produtos = $repProdutos->findBy([
            'tipoProd' => 'arma'
        ]);

        $parametro = $repParametro->find(1);

        $dbh = ibase_connect($host, $username, $password, $charset);

        /* CRIA STRING COM PRODUTOS CONTROLADOS*/
        $csvProdutos = "";
        foreach ($produtos as $produto) {
            if (strlen($csvProdutos) == 0)
                $csvProdutos = "'" . $produto->getCodigo() . "'";
            else {
                $csvProdutos = $csvProdutos . ',' . "'" . $produto->getCodigo() . "'";
            }
        }

        /*PEGA OS PARAMENTROS DO SISTEMA PARA O MES EM ABERTO*/
        $aDate = $parametro->getAnoAbertovendaArma() . '-' . $parametro->getMesAbertoVendaArma() . '-01';
        $dataInicial = $parametro->getMesAbertoVendaArma() . '-01-' . $parametro->getAnoAbertovendaArma();
        $dataFinal = date("m-t-Y", strtotime($aDate));
//        $csvProdutos = "";
//        foreach ($produtos as $produto) {
//            if (strlen($csvProdutos) == 0)
//                $csvProdutos = "'" . $produto->getCodigo() . "'";
//            else {
//                $csvProdutos = $csvProdutos . ',' . "'" . $produto->getCodigo() . "'";
//            }
//        }

        $sqlSelect =
            " select  "
            . " A.DATA as DATA,"
            . " d.codigo,"
            . " c.descricao_produto as descricao,"
            . " c.quantidade as quantidade,"
            . " a.FONE_VINCULO,"
            . " a.nome_vinculo as nome_cliente,"
            . " a.cnpj_vinculo as cnpj_cliente,"
            . " c.ID_ITEMNOTAFISCAL as id_item,"
            . " A.codigo as docfiscal, "
            . " a.tipo_notafiscal as tipo,"
            . " c.INFADPROD as serie,"
            . " a.CEP_VINCULO as cep,"
            . " e.nome AS bairro,"
            . " c2.nome AS cidade,"
            . " a.numero_vinculo AS numero,"
            . " r.nome AS rua,"
            . " a.compl_vinculo AS complemento,"
            . " c2.uf "
            . " from  "
            . "  NOTAFISCAL A,  "
            . "  NATOP B,  "
            . "  item_notafiscal c , "
            . "  produtos d, "
            . " BAIRROS e,"
            . " CIDADES c2,"
            . " RUAS r"
            . " where  "
            . "  1=1 "
            . "  and d.ID_PRODUTO = c.ID_PRODUTO "
            . "  and A.ID_NATOP = B.ID_NATOP "
            . "  and B.CODIGO in ('1.102', '2.102','5.102', '6.102') "
            . "  and A.STATUS = 'S' "
            . "  and c.id_notafiscal = a.id_notafiscal"
            . "  and d.codigo in (" . $csvProdutos . ")"
            . "    and a.data between "
            . " '" . $dataInicial . "' "
            . " and "
            . " '" . $dataFinal . " '"
            . " AND e.id_bairro = a.id_bairro_vinculo"
            . " AND c2.id_cidade = a.id_cidade_vinculo"
            . " AND r.id_rua = a.id_rua_vinculo";


        $query = ibase_query($dbh, $sqlSelect);
//        $caracteresEspeciais = array('-', '.', ',');
        $movimentos = array();
        while ($row = ibase_fetch_object($query)) {

            $movimento = $repMovimento->findBy(['docFiscalItem' => $row->ID_ITEM]);
            if (!$movimento) {
                $movimento = new MapaMovimentoArmas();
                if ($row->TIPO == 'E') {
                    $movimento->setAno($parametro->getAnoAbertovendaArma());
                    $movimento->setMes($parametro->getMesAbertoVendaArma());
                }
                $movimento->setQtde($row->QUANTIDADE);
                $movimento->setDocFiscal($row->DOCFISCAL);
                $movimento->setDocFiscalItem($row->ID_ITEM);
                if (is_null($row->SERIE)) {
                    $movimento->setSerie('');
                } else {
                    $movimento->setSerie(substr((utf8_encode($row->SERIE)), 0, 50));
                }
                $movimento->setDataFiscal(\DateTime::createFromFormat('Y-m-d', $row->DATA));

                $movimento->setTipoMov($row->TIPO);
                $produto = $repProdutos->findOneBy(['codigo' => $row->CODIGO]);
                if ($produto) {
                    $movimento->setProduto($produto);
                }
                $clienteFornecedor = $repClienteFornecedor->findOneBy(['cpf' => $row->CNPJ_CLIENTE]);
                if (!$clienteFornecedor) {
                    $clienteFornecedor = new MapaClienteFornecedor();
                    $clienteFornecedor->setNome(utf8_encode($row->NOME_CLIENTE));
                    $clienteFornecedor->setCpf(str_replace($this->caracteresEspeciais, '', $row->CNPJ_CLIENTE));
                    $clienteFornecedor->setTelefone(str_replace($this->caracteresEspeciais, '', $row->FONE_VINCULO));
                    $movimento->setClienteFornecedor($clienteFornecedor);
                }
                if (!$clienteFornecedor->getEndereco()) {
                    $endClienteFornecdor = new MapaClienteFornecedorEnd();
                    $endClienteFornecdor->setCep($row->CEP);
                    $endClienteFornecdor->setCidade(utf8_encode($row->CIDADE));
                    $endClienteFornecdor->setComplemento(utf8_encode($row->COMPLEMENTO));
                    $endClienteFornecdor->setEndereco(utf8_encode($row->RUA));
                    $endClienteFornecdor->setUf(utf8_encode($row->UF));
                    $endClienteFornecdor->setBairro(utf8_encode($row->BAIRRO));
                    $endClienteFornecdor->setNumero($row->NUMERO);
                    $clienteFornecedor->setEndereco($endClienteFornecdor);
                }
                $movimento->setClienteFornecedor($clienteFornecedor);
                $entityManager->persist($movimento);
                $entityManager->flush();
            }

        }

        ibase_free_result($query);
        ibase_close($dbh);
        $parametro->setUltMovArma(\DateTime::createFromFormat('Y-m-d', date('Y-m-d')));
        $entityManager->persist($parametro);
        $entityManager->flush();

    }

    public function getInventoryAmmo()
    {
        $repMovimento = $this->getDoctrine()->getRepository(MapaMovimento::class);
        $repParametro = $this->getDoctrine()->getRepository(MapaParametro::class);
        $repEstoque = $this->getDoctrine()->getRepository(MapaEstoque::class);
        $parametro = $repParametro->find(1);
        $tipoCalibre = array('Projétil', 'Pólvora', 'Cartucho Vazio', 'Cartucho');
        /* **********Deleta o estoque do mês atual para cria-lo novamente*/
        $repEstoque->deleteMonthYear($parametro->getMesAberto(), $parametro->getAnoAberto(), $tipoCalibre);

        /* ******* cria novo estoque no mês atual com o saldo final do mes anterior*/
        $movimentos = $repMovimento->findByTipo($parametro->getMesAberto(), $parametro->getAnoAberto(), 'municao');
        if ($parametro->getMesAberto() == 1) {
            $estoques = $repEstoque->findByTipoCalibre($parametro->getAnoAberto() - 1, 12, $tipoCalibre);
        } else {
            $estoques = $repEstoque->findByTipoCalibre($parametro->getAnoAberto(), $parametro->getMesAberto() - 1, $tipoCalibre);
        }

    }

    /**
     * @Route("/mapa/cadastro/movimento/municao", name="mapa_cadastro_movimento_municao")
     */
    public function cadastroMovimentoMunicao(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('produto', TextType::class)
            ->add('cliente', TextType::class, [
                'label' => 'Cliente/Fornecedor'
            ])
            ->add('mes', IntegerType::class)
            ->add('ano', IntegerType::class)
            ->add('qtde', IntegerType::class)
            ->add('registro', TextType::class, [
                'required' => false
            ])
            ->add('docFiscal', TextType::class)
            ->add('tipoMov', ChoiceType::class, [
                'choices' => [
                    'Saida' => 'S',
                    'Entrada' => 'E'
                ]
            ])
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $novoMovimento = $form->getData();
            $movimento = new MapaMovimento();
            $movimento->setAno($novoMovimento['ano']);
            $movimento->setMes($novoMovimento['mes']);
            $movimento->setQtde($novoMovimento['qtde']);
            $movimento->setDocFiscal($novoMovimento['docFiscal']);
            $movimento->setDocFiscalItem(0);
            $movimento->setTipoMov('S');
            if ($novoMovimento['registro'] == null) {
                $movimento->setRegistro('Compra de Mercadoria');
            } else {
                $movimento->setRegistro($novoMovimento['registro']);
            }
            $movimento->setClienteFornecedor($this->getDoctrine()
                ->getRepository(MapaClienteFornecedor::class)
                ->find($novoMovimento['cliente']));
            $movimento->setProduto($this->getDoctrine()
                ->getRepository(MapaProduto::class)
                ->find($novoMovimento['produto']));
            try {
                $entityManager->persist($movimento);
                $entityManager->flush();
                $this->addFlash(
                    'notice',
                    'Atualizações salvas com sucesso! '
                );
            } catch (\Exception $exception) {
                $this->addFlash(
                    'error',
                    'Falha ao atualizar registros! ' . $form->getErrors(true)
                );
            };


        }

        return $this->render('default/mapa_eb/cadastro_movimento_municao.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/mapa/movimentos/mes", name="mapa_movimentos_mes",methods={"GET"})
     */

    public function getMovimentosMesAmmo(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $repMovimento = $this->getDoctrine()->getRepository(MapaMovimento::class);
            $repParametro = $this->getDoctrine()->getRepository(MapaParametro::class);
            $parametro = $repParametro->find(1);
            $movimentosMes = $repMovimento->findByTipo($parametro->getMesAberto(),
                $parametro->getAnoAberto(),
                'municao'
            );

            $movArray = array();
            foreach ($movimentosMes as $mov) {
                array_push($movArray, array(
                    "id" => $mov->getId(),
                    "cliente" => $mov->getClienteFornecedor()->getNome(),
                    "produto" => $mov->getProduto()->getDescricao(),
                    "pedidoNota"=> $mov->getDocFiscal(),
                    "registro" => $mov->getRegistro(),
                    "qtde" => $mov->getQtde()
                ));
            }
            return new JsonResponse($movArray);
        }
        return new Response('This is not Ajax', 200);


    }

    /**
     * @Route("/mapa/movimentos/mes/armas", name="mapa_movimentos_mes_armas",methods={"GET"})
     */

    public function getMovimentosMesGun(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $repMovimento = $this->getDoctrine()->getRepository(MapaMovimentoArmas::class);
            $repParametro = $this->getDoctrine()->getRepository(ParametrosMapa::class);
            $parametro = $repParametro->find(1);
            $dataInicial = $parametro->getAnoAbertovendaArma() . '-' . $parametro->getMesAbertoVendaArma()
                . '-01';
            $dataFinal = $parametro->getAnoAbertovendaArma() . '-' . $parametro->getMesAbertoVendaArma()
                . '-'
                . cal_days_in_month(
                    CAL_GREGORIAN,
                    $parametro->getMesAbertoVendaArma(),
                    $parametro->getAnoAbertovendaArma()
                );
            $entSaida = array('S', 'E');
            $movimentosMes = $repMovimento->findByDocFiscalDate(
                $dataInicial,
                $dataFinal,
                $entSaida
            );

            $movArray = array();

            foreach ($movimentosMes as $mov) {
                array_push($movArray, array(
                    "id" => $mov->getId(),
                    "cliente" => $mov->getClienteFornecedor()->getNome(),
                    "produto" => $mov->getProduto()->getDescricao(),
                    "dataFiscal" => $mov->getDataFiscal()->format('d/m/Y'),
                    "entSaida" => $mov->getTipoMov(),
                    "serie" => $mov->getSerie(),
                    "qtde" => $mov->getQtde()
                ));
            }
            return new JsonResponse($movArray);
        }
        return new Response('This is not Ajax', 200);


    }

    /**
     * @Route("/mapa/movimentos/update", name="mapa_movimentos_update",methods={"PUT"})
     */
    public function setRegistroAmmo(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $id = $request->request->get('id');
            $registro = $request->request->get('registro');
            $repMovimento = $this->getDoctrine()->getRepository(MapaMovimento::class);
            $mov = $repMovimento->find(intval($id));
            if ($mov) {
                try {
                    $mov->setRegistro($registro);
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($mov);
                    $entityManager->flush();
                    $response = new Response('updated', 200);
                } catch (Exception $e) {
                    $response = new Response($e->getMessage(), 500);
                }

            } else {
                $response = new Response('not found', 400);
            }

            return $response;
        }
        return new Response('This is not Ajax', 405);
    }

    /**
     * @Route ("/mapa/movimento/arma/serie",name="mapa_movimento_arma_serie", methods={"PUT"})
     */
    public function atualizaSerie(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $id = $request->request->get('id');
            $serie = $request->request->get('serie');
            $repMovimento = $this->getDoctrine()->getRepository(MapaMovimentoArmas::class);
            $mov = $repMovimento->find(intval($id));
            if ($mov) {
                try {
                    $mov->setSerie($serie);
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($mov);
                    $entityManager->flush();
                    return new JsonResponse("ok", 200);
                } catch (Exception $e) {
                    return new JsonResponse($e->getMessage(), 500);
                }

            } else {
                return new JsonResponse('not found', 400);
            }
        }
        return new JsonResponse('This is not Ajax', 405);
    }

    private function getEndereco(string $cpf)
    {


        $sql = 'SELECT
                REPLACE(replace(REPLACE(c.CNPJ, \' - \', \'\'), \' . \', \'\'), \' / \', \'\') AS cnpj,
                r2.NOME AS rua,
                c.NUMERO,
                c.compl,
                b2.NOME AS bairro,
                c2.NOME AS cidade, 
                c2.UF,
                c.cep
            FROM
                CLIENTES c,
                CIDADES c2,
                RUAS r2 ,
                BAIRROS b2 
            WHERE
                1 = 1
                AND REPLACE(replace(REPLACE(c.CNPJ, \' - \', \'\'), \' . \', \'\'), \' / \', \'\') = \'' . $cpf . '\'
                AND c2.ID_CIDADE = c.ID_CIDADE
                AND b2.ID_BAIRRO =c.ID_BAIRRO
                AND r2.ID_RUA = c.ID_RUA';
        $endClientefornecedor = new MapaClienteFornecedorEnd();

        try {
            $dbh = ibase_connect($this->host, $this->username, $this->password, $this->charset);
            $query = ibase_query($dbh, $sql);
            while ($row = ibase_fetch_object($query)) {
                $endClientefornecedor->setBairro(utf8_encode($row->BAIRRO));
                $endClientefornecedor->setCidade(utf8_encode($row->CIDADE));
                $endClientefornecedor->setEndereco(utf8_encode($row->RUA));
                $endClientefornecedor->setNumero($row->NUMERO);
                $endClientefornecedor->setUf($row->UF);
                if ($row->CEP) {
                    $endClientefornecedor->setCep(str_replace($this->caracteresEspeciais, '', $row->CEP));
                } else {
                    $endClientefornecedor->setCep('00000000');
                }
                $endClientefornecedor->setComplemento($row->COMPL);
                return $endClientefornecedor;
            }
            return null;

        } catch (\Exception $exception) {
            return $endClientefornecedor;
        }


    }

}
