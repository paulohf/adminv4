<?php


namespace App\Controller\Mapa;


use App\Entity\MapaClienteFornecedor;
use App\Entity\MapaClienteFornecedorEnd;
use App\Entity\MapaMovimento;
use App\Entity\MapaProduto;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\Routing\Annotation\Route;

class APIMapaController extends AbstractController
{
    private $caracteresEspeciais = array('-', '.', ',');

    public function __construct(HttpClientInterface $tinyClient)
    {
        $this->client = $tinyClient;
    }

    /**
     * @Route ("/mapa/importar/nota_municao",name="mapa_importar_nota_municao")
     */
    public function importarNota(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $entityManager = $this->getDoctrine()->getManager();
            $repMapaProduto = $this->getDoctrine()->getRepository(MapaProduto::class);
            $repMapaCliente = $this->getDoctrine()->getRepository(MapaClienteFornecedor::class);
            $repMapaMovimento = $this->getDoctrine()->getRepository(MapaMovimento::class);

            $numeroPedidoRequest = $request->request->get('notafiscal');
            $tipoMovimento = $request->request->get('es');

            $mapaMovimento = $repMapaMovimento->findBy([
                'docFiscal' => 'P' . $numeroPedidoRequest
            ]);
            if ($mapaMovimento) {
                return new JsonResponse('Movimento já cadastrado', 200);
            }

            if ($numeroPedidoRequest <> "") {
                /*ENCONTRA O ID DO PEDIDO PELO SEU NUMERO*/
                $responsePedidos = $this->client
                    ->request('GET', '/api2/pedidos.pesquisa.php', [
                        'query' => [
                            'numero' => $numeroPedidoRequest,
                        ]
                    ]);
                $content = $responsePedidos->toArray();
                $retorno = $content['retorno'];
                if ($retorno['status'] == 'OK') {
                    $idPedido = $retorno["pedidos"][0]["pedido"]["id"];
                    /* ENCONTRA PEDIDO PELO SEU ID*/
                    $responsePedido = $this->client
                        ->request('GET', '/api2/pedido.obter.php', [
                            'query' => [
                                'id' => $idPedido,
                            ]
                        ]);
                    $conteudoNota = $responsePedido->toArray();
                    $pedidoTiny = $conteudoNota['retorno'];
                    if ($pedidoTiny['status'] == 'OK') {
                        foreach ($pedidoTiny['pedido']['itens'] as $itens) {
                            $mapaProduto = $repMapaProduto->findOneBy([
                                    'codigo' => $itens['item']['codigo']
                                ]
                            );
                            if ($mapaProduto) {
                                $mapaMovimento = new MapaMovimento();
                                $mapaMovimento->setProduto($mapaProduto);
                                $mapaMovimento->setRegistro($pedidoTiny['pedido']['obs_interna']);
                                $mapaMovimento->setDocFiscal('P' . $pedidoTiny['pedido']['numero']);
                                $mapaMovimento->setDocFiscalItem($itens['item']['id_produto']);
                                $mapaMovimento->setTipoMov('S');
                                $mapaMovimento->setQtde($itens['item']['quantidade']);
                                $dataEmissaoPedido = \DateTime::createFromFormat(
                                    'd/m/Y',
                                    $pedidoTiny['pedido']['data_faturamento']
                                );
                                $mapaMovimento->setMes($dataEmissaoPedido->format('m'));
                                $mapaMovimento->setAno($dataEmissaoPedido->format('Y'));
                                $mapaCliente = $repMapaCliente->findOneBy([
                                    'cpf' => str_replace($this->caracteresEspeciais, '', $pedidoTiny['pedido']['cliente']['cpf_cnpj'])
                                ]);
                                if ($mapaCliente) {
                                    /**
                                     * TODO: verificar se o cliente existe e se os dados estão atualizados
                                     */
                                    $mapaMovimento->setClienteFornecedor($mapaCliente);
                                } else {
                                    $mapaCliente = new MapaClienteFornecedor();
                                    $mapaCliente->setNome($pedidoTiny['pedido']['cliente']['nome']);
                                    $mapaCliente->setTelefone($pedidoTiny['pedido']['cliente']['fone']);
                                    $mapaCliente->setCpf($pedidoTiny['pedido']['cliente']['cpf_cnpj']);
                                    $mapaClienteEndereco = new MapaClienteFornecedorEnd();
                                    $mapaClienteEndereco->setClienteFornecedor($mapaCliente);
                                    $mapaClienteEndereco->setCep(
                                        str_replace($this->caracteresEspeciais, '', $pedidoTiny['pedido']['cliente']['cep'])
                                    );
                                    $mapaClienteEndereco->setBairro($pedidoTiny['pedido']['cliente']['bairro']);
                                    $mapaClienteEndereco->setCidade($pedidoTiny['pedido']['cliente']['cidade']);
                                    $mapaClienteEndereco->setUf($pedidoTiny['pedido']['cliente']['uf']);
                                    $mapaClienteEndereco->getEndereco($pedidoTiny['pedido']['cliente']['endereco']);
                                    $mapaClienteEndereco->setComplemento($pedidoTiny['pedido']['cliente']['complemento']);
                                    $mapaMovimento->setClienteFornecedor($mapaCliente);
                                }
                                $entityManager->persist($mapaMovimento);
                                $entityManager->flush();
                            }

                        }


                    }


                }
            }
            return new Response("This is not AJAX", 200);

        }

    }
}