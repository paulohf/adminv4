<?php

namespace App\Controller\Mapa;


use App\Entity\MapaClienteFornecedor;
use App\Entity\MapaClienteFornecedorEnd;
use phpDocumentor\Reflection\Types\False_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;


class MapaClienteController extends AbstractController
{
    private $caracteresEspeciais = array('-', '.', ',');
    private $host = 'localhost:C:\\Forti Up\\DADOS\\AOPESCADOR.FDB';
    private $charset = 'ISO8859_9';
    private $username = 'SYSDBA';
    private $password = 'masterkey';

    /**
     * @Route("/mapa/clientes", name="mapa_clientes")
     */
    public function index()
    {
        $repository = $this->getDoctrine()
            ->getRepository(MapaClienteFornecedor::class);
        $clientes = $repository->findAll();

        return $this->render('default/mapa_eb/lista_mapa_clientes.twig', [
            'controller_name' => 'MapaCalibreController', 'clientes' => $clientes
        ]);
    }


    /**
     * @Route("mapa/clientes/resincronizar", name="mapa_clientes_resincronizar")
     */
    public function resincronizarClientes()
    {

        $repClientes = $this->getDoctrine()
            ->getRepository(MapaClienteFornecedor::class);
        $clientes = $repClientes->findAll();
        $em = $this->getDoctrine()->getManager();

        foreach ($clientes as $cliente) {

            $sql = 'SELECT
                    c.codigo,
                    c.nome,
                    REPLACE(replace(REPLACE(c.CNPJ, \' - \', \'\'), \' . \', \'\'), \' / \', \'\') AS cnpj,
                    c.CEP,
                    c.compl,
                    c.FONE,
                    c.NUMERO,
                    c2.uf,
                    c2.nome AS cidade,
                    b.nome AS bairro,
                    r.nome AS rua
                FROM
                    CLIENTES c,
                    CIDADES c2,
                    BAIRROS b,
                    RUAS r
                WHERE
                    1 = 1
                    AND c.ID_CIDADE = c2.id_cidade
                    AND b.id_bairro = c.ID_BAIRRO
                    AND c.ID_RUA = r.id_rua
                    AND trim(REPLACE(replace(REPLACE(c.CNPJ, \'-\', \'\'), \'.\', \'\'), \'/\', \'\')) = \'' . $cliente->getCpf() . '\'';
            try {
                $dbh = ibase_connect($this->host, $this->username, $this->password, $this->charset);
                $query = ibase_query($dbh, $sql);
                while ($row = ibase_fetch_object($query)) {
                    if ($row->FONE <> null) {
                        $cliente->setTelefone($row->FONE);
                    }

                    if (!is_null($cliente->getEndereco())) {
                        $endereco = $cliente->getEndereco();
                    } else {
                        $endereco = new MapaClienteFornecedorEnd();
                    }
                    $endereco->setCep($row->CEP);
                    $endereco->setEndereco(utf8_encode($row->RUA));
                    $endereco->setNumero($row->NUMERO);
                    $row->COMPL <> null ? $endereco->setComplemento($row->COMPL) : false;
                    $endereco->setUf($row->UF);
                    $endereco->setCidade(utf8_encode($row->CIDADE));
                    $endereco->setBairro(utf8_encode($row->BAIRRO));
                    $cliente->setEndereco($endereco);
                    $em->persist($cliente);
                    $em->flush();
                }

            } catch (\Exception $exception) {

            }

        }
        return $this->render('default/page_blank.twig');
    }

}
