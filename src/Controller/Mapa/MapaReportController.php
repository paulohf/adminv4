<?php

namespace App\Controller\Mapa;

use App\Entity\MapaEstoque;
use App\Entity\MapaMovimento;
use App\Entity\MapaMovimentoArmas;
use App\Entity\ParametrosMapa;
use App\Form\NotaFiscalArmasType;
use PHPJasper\PHPJasper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\HttpFoundation\File\File;
use App\Entity\NotaFiscalMapa;


class MapaReportController extends AbstractController
{
    /**
     * @Route("MapaReporteVendasPorTipo",name="MapaReporteVendasPorTipo")
     *
     */
    public function mapaReporteVendasPorTipoAction(Request $request)
    {
        $reportParam = array();
        $form = $this->createFormBuilder($reportParam, array('attr' => array('target' => '_blank')))
            ->add('mes', IntegerType::class, array('constraints' => new Length(array(
                'min' => 1, 'max' => 12,
                'maxMessage' => 'Mês não pode ser maior do que {{ limit }}'
            ))))
            ->add('ano', IntegerType::class)
            ->add('data', DateType::class, array(
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'date'],
                'format' => 'dd/MM/yyyy',
            ))
            ->add('tipo', ChoiceType::class, array('choices' => array(
                'Inventário de Munições' => 'Inventario',
                'Vendas a Clientes' => 'Clientes',
            )))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                $data = $form->getData();
                // return $this->file($this->printReportMunicao($template[$data['tipo']], $data['tipo'], $data['ano'], $data['mes'], $data['data']));
                if ($data['tipo'] === 'Inventario') {
                    $tipoCalibre = array('Projétil', 'Pólvora', 'Cartucho Vazio', 'Cartucho');
                    $inventario = $this->getDoctrine()
                        ->getRepository(MapaEstoque::class)
                        ->findByTipoCalibre(
                            $data['ano'],
                            $data['mes'],
                            $tipoCalibre
                        );
                    if ($inventario) {
                        return $this->render(
                            'default/mapa_eb/relatorio_venda_municao_inventario.twig',
                            array('inventario' => $inventario,
                                'mes' => $data['mes'],
                                'ano' => $data['ano'],
                                'data' => $data['data']
                            )
                        );
                    } else {
                        $this->addFlash(
                            'error',
                            'Nenhum registro encontrado! ' . $form->getErrors(true)
                        );
                    }
                } else {
                    $movClientes = $this->getDoctrine()
                        ->getRepository(MapaMovimento::class)
                        ->findBy(['ano' => $data['ano'],
                            'mes' => $data['mes'],
                            'tipoMov' => 'S',
                        ],
                            ['id' => 'DESC']
                        );
                    if ($movClientes) {
                        return $this->render(
                            'default/mapa_eb/relatorio_venda_munição_clientes.twig',
                            array('movimentos' => $movClientes,
                                'mes' => $data['mes'],
                                'ano' => $data['ano'],
                                'data' => $data['data']
                            )
                        );
                    } else {
                        $this->addFlash(
                            'error',
                            'Nenhum registro encontrado! ' . $form->getErrors(true)
                        );
                    }

                }
            }
        }


        return $this->render('default/mapa_eb/page_imprimir_mapa.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("MapaReportVendaArma",name="MapaReportVendaArma")
     */
    public function mapaReporteVendaArmas(Request $request)
    {
        $teste = array();
        $form = $this->createFormBuilder($teste, array('attr' => array('target' => '_blank')))
            ->add('mes', IntegerType::class, array('label' => 'Mês'))
            ->add('ano', IntegerType::class, array('label' => 'Ano'))
            ->add('dataRelatorio', DateType::class, array(
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd/MM/yyyy',
                'label' => 'Data Relatório'
            ))
            ->add('tipo', ChoiceType::class, array('choices' => array(
                'Inventário de Armas' => 'Inventario',
                'Vendas a Clientes' => 'Clientes',
            )))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                $data = $form->getData();

                if ($data['tipo']=='Clientes') {
                    $dataInicial = $data['ano'] . '-' . $data['mes'] . '-01';
                    $dataFinal = $data['ano'] . '-' . $data['mes']
                        . '-'
                        . cal_days_in_month(CAL_GREGORIAN, $data['mes'], $data['ano']);
                    $entSaida = array('S');
                    $mapaMovimentoArmas = $this->getDoctrine()
                        ->getRepository(MapaMovimentoArmas::class)
                        ->findByDocFiscalDate(
                            $dataInicial,
                            $dataFinal,
                            $entSaida
                        );
                    if ($mapaMovimentoArmas) {
                        return $this->render(
                            'default/mapa_eb/relatorio_venda_armas_clientes.twig',
                            array('movimentos' => $mapaMovimentoArmas,
                                'mes' => $data['mes'],
                                'ano' => $data['ano'],
                                'data' => $data['dataRelatorio']
                            )
                        );
                    } else {
                        $this->addFlash(
                            'error',
                            'Não foram encontrados registros ' . $form->getErrors(true)
                        );
                    }
                }else{
                    $repMapaEstoque = $this->getDoctrine()->getRepository(MapaEstoque::class);
                    $tipoCalibre = array('Arma');
                    $mapaEstoques = $repMapaEstoque->findByTipoCalibre(
                        $data['ano'],
                        $data['mes'],
                        $tipoCalibre
                    );
                    if($mapaEstoques){
                        return $this->render(
                            'default/mapa_eb/relatorio_venda_armas_inventario.twig',
                            array('inventario' => $mapaEstoques,
                                'mes' => $data['mes'],
                                'ano' => $data['ano'],
                                'data' => $data['dataRelatorio']
                            )
                        );

                    }else{
                        $this->addFlash(
                            'error',
                            'Não foram encontrados registros ' . $form->getErrors(true)
                        );
                    }

                }

            } else {
                $this->addFlash(
                    'error',
                    'Falha ao imprimir registros! ' . $form->getErrors(true)
                );
            }
        }


        return $this->render('default/mapa_eb/page_imprimir_mapa_venda_armas.twig', array('form' => $form->createView()));
    }

//    /**
//     * @Route("DemonstrativoVendaArmas",name="DemonstrativoVendaArmas")
//     *
//     */
//    //*************** Função obsoleta **********************************
//    public function demonstrativoVendaArmasAction(Request $request)
//    {
//        $host = 'localhost:C:\\Forti Up\\DADOS\\AOPESCADOR.FDB';
//        $charset = 'ISO8859_9';
//        $username = 'SYSDBA';
//        $password = 'masterkey';
//        $notas = array();
//        try {
//            $em = $this->getDoctrine()->getRepository(ParametrosMapa::class);
//            $mapaParam = $em->find(1);
//            $dbh = ibase_connect($host, $username, $password, $charset);
//            $sql = '  select
//                        a.nome_vinculo,a.cnpj_vinculo, b.codigo_produto, b.descricao_produto, b.infadprod,
//                        a.data, (d.nome || \',\'|| a.numero_vinculo || \', \' || e.nome || \', \' || f.nome || \', \' || f.uf )
//                        as endereco, a.codigo
//
//                    from
//                        notafiscal a, item_notafiscal b, natop c, ruas d, bairros e, cidades f
//                    where 1=1
//                        and a.id_notafiscal = b.id_notafiscal
//                        and a.status = \'S\'
//                        and c.id_natop = a.id_natop
//                        and c.codigo in (\'5.101\',\'5.102\',\'6.101\',\'6.102\')
//                        and b.infadprod is not NULL
//                        and d.id_rua = a.id_rua_vinculo
//                        and e.id_bairro = a.id_bairro_vinculo
//                        and f.id_cidade = a.id_cidade_vinculo
//                        and a.data between ? and ?';
//            $query = ibase_prepare($dbh, $sql);
//
//            $result = ibase_execute(
//                $query,
//                date('m-1-Y', strtotime($mapaParam->getAnoAbertovendaArma() . '-' . $mapaParam->getMesAbertoVendaArma() . '-01')),
//                date('m-t-Y', strtotime($mapaParam->getAnoAbertovendaArma() . '-' . $mapaParam->getMesAbertoVendaArma() . '-01'))
//            );
//
//            while ($row = ibase_fetch_object($result)) {
//                $nota = new NotaFiscalMapa();
//                $nota->setNome(utf8_encode($row->NOME_VINCULO));
//                $nota->setCpf($row->CNPJ_VINCULO);
//                $nota->setProduto(utf8_encode($row->DESCRICAO_PRODUTO));
//                $nota->setSerial(utf8_encode(substr($row->INFADPROD, 0, 50)));
//                $nota->setData(new \DateTime($row->DATA));
//                $nota->setEndereco(utf8_encode($row->ENDERECO));
//                $nota->setNotafiscal($row->CODIGO);
//                array_push($notas, $nota);
//            }
//
//            $form = $this->createForm(NotaFiscalArmasType::class, array('notas' => $notas));
//
//            $form->get('mes')->setData($mapaParam->getMesAbertoVendaArma());
//            $form->get('ano')->setData($mapaParam->getAnoAbertovendaArma());
//
//            $form->handleRequest($request);
//        } catch (Exception $e) {
//            echo $e->getMessage();
//        }
//
//        if ($form->isSubmitted()) {
//            if ($form->isValid()) {
//                $em = $this->getDoctrine()->getManager();
//                $retorno = $form->getData();
//
//                $vendas = $retorno["notas"];
//
//                foreach ($vendas as $key => $venda) {
//                    if ($venda->getFabricante() <> null) {
//                        $notaFiscal = new NotaFiscalMapa();
//                        $notaFiscal->setNome($venda->getNome());
//                        $notaFiscal->setProduto($venda->getProduto());
//                        $notaFiscal->setCpf($venda->getCpf());
//                        $notaFiscal->setData($venda->getCpf());
//                        $notaFiscal->setFabricante($venda->getFabricante());
//                        $notaFiscal->setSerial($venda->getSerial());
//                        $notaFiscal->setData($venda->getData());
//                        $notaFiscal->setEndereco($venda->getEndereco());
//                        $notaFiscal->setNotafiscal($venda->getNotafiscal());
//                        $em->persist($notaFiscal);
//                    }
//                }
//
//                if ($mapaParam->getMesAbertoVendaArma() < 12) {
//                    $mapaParam->setMesAbertoVendaArma($mapaParam->getMesAbertoVendaArma() + 1);
//                } else {
//                    //                      ****  Novo ano  ****
//                    $mapaParam->setMesAbertoVendaArma(1);
//                    $mapaParam->getAnoAbertovendaArma($mapaParam->getAnoAbertovendaArma() + 1);
//                }
//                $em->persist($mapaParam);
//
//                $em->flush();
//                return $this->redirectToRoute('DemonstrativoVendaArmas');
//            } else {
//                $this->addFlash(
//                    'error',
//                    'Falha ao atualizar registros! ' . $form->getErrors(true)
//                );
//            }
//        }
//
//
//        return $this->render('default/mapa_eb/page_demons_venda_armas.twig', array('form' => $form->createView()));
//    }


//    public function printReportMunicao($template, $tipo, $ano, $mes, $data)
//    {
//        $report = new PHPJasper();
//
//        $input = $_SERVER["DOCUMENT_ROOT"] . '\reports' . "\\" . $template . '.jasper';
//        $options = [
//            'format' => ['pdf'],
//            'locale' => 'pt_BR',
//            'params' => ['data_relatorio' => date_format($data, 'd/m/Y'), 'mes' => $mes, 'ano' => $ano],
//            'db_connection' => [
//                'driver' => 'mysql',
//                'username' => 'root',
//                'password' => 'templar',
//                'host' => '127.0.0.1',
//                'database' => 'financeiro',
//                'port' => '3306',
//                'jdbc_url' => 'jdbc:mysql://localhost:3306/financeiro',
//                'jdbc_dir' => $_SERVER["DOCUMENT_ROOT"] . '\..\vendor\geekcom\phpjasper\bin\jasperstarter\jdbc'
//
//            ]
//        ];
//        $output = $_SERVER["DOCUMENT_ROOT"] . '\reports' . '\Venda Munição - ' .
//            $tipo . ' ' . JDMonthName(gregoriantojd($mes, 1, $ano), 0) . '-' . $ano;
//
//        /*       $report->process(
//            $input,
//            $output,
//            $options
//        )->execute(); */
//        $report->process(
//            $input,
//            $output,
//            $options
//        )->output();
//
//
//    }


}
