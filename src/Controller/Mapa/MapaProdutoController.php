<?php

namespace App\Controller\Mapa;

use App\Entity\MapaProduto;
use App\Form\MapaProdutoType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MapaProdutoController extends AbstractController
{
    /**
     * @Route("/mapa/produto", name="mapa_produto")
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(MapaProduto::class);
        $produtos = $repository->findAll();
        return $this->render('default/mapa_eb/lista_mapa_produto.twig', [
            'controller_name' => 'MapaProdutoController', 'produtos' => $produtos
        ]);
    }

    /**
     * @Route ("mapa/produto/novo", name="mapa_novo_produto")
     */
    public function novoProduto(Request $request)
    {
        $MapaProduto = new MapaProduto();
        $form = $this->createForm(MapaProdutoType::class, $MapaProduto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $produto = $form->getData();
                $entity = $this->getDoctrine()->getManager();
                $entity->persist($produto);
                $entity->flush();
                $this->addFlash(
                    'notice',
                    'Atualizações salvas com sucesso! '
                );
            }catch (Exception $e){
                $this->addFlash(
                    'error',
                    'Falha ao salvar registros! ' . $form->getErrors(true)
                );
            }
        }
        return $this->render('default/mapa_eb/cadastro_produtos.twig', [
            'form' => $form->createView(),
        ]);

    }
}
