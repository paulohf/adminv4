<?php

namespace App\Controller\Mapa;

use App\Entity\MapaCalibre;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class MapaCalibreController extends AbstractController
{
    /**
     * @Route("/mapa/calibre", name="mapa_calibre")
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(MapaCalibre::class);
        $calibres = $repository->findAll();

        return $this->render('default/mapa_eb/lista_mapa_calibre.twig', [
            'controller_name' => 'MapaCalibreController', 'calibres' => $calibres
        ]);
    }
}
