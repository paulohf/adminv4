<?php

namespace App\Controller;

use PhpParser\Node\Expr\Array_;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class MovimentosEstController extends AbstractController
{
    private $host = 'localhost:C:\\Forti Up\\DADOS\\AOPESCADOR.FDB';
    private $username = 'SYSDBA';
    private $password = 'masterkey';


    /**
     * @Route("/movimentos/est", name="movimentos_est")
     */
    public function index()
    {

        return $this->render('default/movimentos_est/mov_nf_entrada.html.twig', [
            'controller_name' => 'MovimentosEstController',
        ]);
    }


    /**
     * @Route("/movimentos/est/mult", name="movimentos_est_mult")
     */
    public function movimentosMult()
    {

        return $this->render('default/movimentos_est/mov_nf_entrada_mult.html.twig', [
            'controller_name' => 'MovimentosEstController',
        ]);
    }

    /**
     * @Route("/v1/estoque/entradas",name="v1_estoque_mov_ent")
     *
     */
    public function getMovEnt(Request $request)
    {
        $arrayMes = Array();
        $arrayQuantidade = Array();
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {


            try {

                $ano = $request->request->get('ano');
                $codigo = $request->request->get('codigo');
                $dbh = ibase_connect($this->host, $this->username, $this->password);
                $stmt = '
 select
      max(vendano.mes) as mes,
      sum(vendano.quantidade) as qtde

          from (            
            select
                g.codigo,
                f.descricao,
                f.quantidade,
                f.valor_liq,
                EXTRACT (month from a.data) as mes
            from
                CUPOM_VEND A,
                MOV_CAIXA C,
                itemcupom_vend f,
                produtos g
            where
                1=1
                and g.id_produto = f.id_produto
                and f.id_cupom_vend = a.id_cupom_vend
                and C.TIPO_PG <> \'TRC\'
                and A.ID_CUPOM_VEND = C.ID_ORIGEM
                and A.CUPOM_CANCELADO = \'N\'
               
                and extract(year from a.data) = \'' . $ano . '\'
                and g.codigo = \'' . $codigo . '\'
            
            union all
            
            select
                itprod.codigo,
                itprod.descricao,
                itped.quantidade,
                itped.valor,
                extract(month from itped.data) as mes
            from
                pedido ped, itempedido itped, produtos itprod
            where
                1=1
                and itprod.id_produto = itped.id_produto
                and itped.id_pedido = ped.id_pedido
               
                and extract(year from ped.data) = \'' . $ano . '\'
                and itprod.codigo = \'' . $codigo . '\'
                and ped.statuspag = \'N\'
                and ped.tipo_pedido = \'P\'
            
            union all
                                        
            select
                prodnf.codigo,
                prodnf.descricao,
                itnota.quantidade,
                itnota.valor,
                extract(month from nota.data) as mes
            from
                NOTAFISCAL nota,
                NATOP B,
                item_notafiscal itnota,
                produtos prodnf
            where
                1=1
                and itnota.id_notafiscal = nota.id_notafiscal
                and prodnf.id_produto = itnota.id_produto
                and nota.ID_NATOP = B.ID_NATOP
                and B.CODIGO in (\'5.102\', \'6.102\', \'1.202\', \'2.202\')
                and nota.STATUS = \'S\'
               
                and extract (year from nota.DATASAIDA) =  \'' . $ano . '\'
                and prodnf.codigo = \'' . $codigo . '\'
                and nota.nome_arq_nfe is not null
        )  as vendano

        group by vendano.mes
            ';

                $sth = ibase_query($dbh, $stmt);
                while ($row = ibase_fetch_object($sth)) {

                    $arrayQuantidade[] = round($row->QTDE);
                    $arrayMes[] = $row->MES;

                }

                ibase_free_result($sth);
                ibase_close($dbh);

            } catch (Exception $e) {
                return $response = new Response(
                    $e,
                    500,
                    ['content-type' => 'text/html']
                );
            }


            return new jsonResponse (array('mes' => $arrayMes, 'qtde' => $arrayQuantidade));

        }
        return new Response('This is not ajax!', 400);
    }

    /**
     * @Route("/v1/estoque/entradas/mult",name="v1_estoque_mov_ent_mult")
     *
     */
    public function getMovEntMult(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {

            try {
                $ano = $request->request->get('ano');
                $descricao = $request->request->get('descricao');
                $dbh = ibase_connect($this->host, $this->username, $this->password);
                $stmt = '
      select
      max(vendano.mes) as mes,
      max(vendano.codigo) as codigo,
      max(vendano.descricao) as descricao,
      sum(vendano.quantidade) as qtde

          from (
                
                select
                    g.codigo,
                    f.descricao,
                    f.quantidade,
                    f.valor_liq,
                    extract(month from a.data) as mes
                from
                    CUPOM_VEND A,
                    MOV_CAIXA C,
                    itemcupom_vend f,
                    produtos g
                where
                    1=1
                    and g.id_produto = f.id_produto
                    and f.id_cupom_vend = a.id_cupom_vend
                    and C.TIPO_PG <> \'TRC\'
                    and A.ID_CUPOM_VEND = C.ID_ORIGEM
                    and A.CUPOM_CANCELADO = \'N\'
    
                    and extract(year from a.data) = \'' . $ano . '\'
                    and g.descricao like \'' . $descricao . '\'
                
                union all
                
                select
                    itprod.codigo,
                    itprod.descricao,
                    itped.quantidade,
                    itped.valor,
                    extract(month from itped.data) as mes
                from
                    pedido ped, itempedido itped, produtos itprod
                where
                    1=1
                    and itprod.id_produto = itped.id_produto
                    and itped.id_pedido = ped.id_pedido
    
                    and extract(year from ped.data) = \'' . $ano . '\'
                    and itprod.descricao like \'' . $descricao . '\'
                    and ped.statuspag = \'N\'
                    and ped.tipo_pedido = \'P\'
                
                union all
                                            
                select
                    prodnf.codigo,
                    prodnf.descricao,
                    itnota.quantidade,
                    itnota.valor,
                    extract(month from nota.data) as mes
                from
                    NOTAFISCAL nota,
                    NATOP B,
                    item_notafiscal itnota,
                    produtos prodnf
                where
                    1=1
                    and itnota.id_notafiscal = nota.id_notafiscal
                    and prodnf.id_produto = itnota.id_produto
                    and nota.ID_NATOP = B.ID_NATOP
                    and B.CODIGO in (\'5.102\', \'6.102\', \'1.202\', \'2.202\')
                    and nota.STATUS = \'S\'
    
                    and extract (year from nota.DATASAIDA) =  \'' . $ano . '\'
                    and prodnf.descricao like \'' . $descricao . '\'
                    and nota.nome_arq_nfe is not null
                     ) as vendano

         group by vendano.codigo, vendano.mes order by vendano.codigo, vendano.mes
                
                
                ';

                $arrayCodigo = array();
                $sth = ibase_query($dbh, $stmt);
                while ($row = ibase_fetch_object($sth)) {
                    if (in_array($row->CODIGO, array_column($arrayCodigo, "CODIGO"))) {
                        $key = array_search($row->CODIGO, array_column($arrayCodigo, "CODIGO"));
                        $arrayCodigo[$key]["M" . $row->MES] = $row->QTDE;
                    } else {
                        $arrayCodigo[] = array(
                            "CODIGO" => $row->CODIGO,
                            "DESCRICAO" => utf8_encode($row->DESCRICAO),
                            "M1" => 0,
                            "M2" => 0,
                            "M3" => 0,
                            "M4" => 0,
                            "M5" => 0,
                            "M6" => 0,
                            "M7" => 0,
                            "M8" => 0,
                            "M9" => 0,
                            "M10" => 0,
                            "M11" => 0,
                            "M12" => 0
                        );
                        $key = array_search($row->CODIGO, array_column($arrayCodigo, "CODIGO"));
                        $arrayCodigo[$key]["M" . $row->MES] = $row->QTDE;


                    }


                }

                ibase_free_result($sth);
                ibase_close($dbh);
                return new JsonResponse($arrayCodigo);

            } catch (Exception $exception) {
                return $response = new Response(
                    $exception,
                    500,
                    ['content-type' => 'text/html']
                );
            };
            return new jsonResponse ($arrayCodigos);

        }
        return new Response('This is not ajax!', 400);
    }

}
