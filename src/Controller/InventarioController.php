<?php

namespace App\Controller;

use PhpParser\Node\Expr\Array_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InventarioController extends AbstractController
{
    private $host = 'localhost:C:\\Forti Up\\DADOS\\AOPESCADOR.FDB';
    private $username = 'SYSDBA';
    private $password = 'masterkey';
    private $charset = 'ISO8859_9';

    /**
     * @Route("/inventario", name="inventario")
     */
    public function index(): Response
    {
        return $this->render('inventario/inventario.html.twig', [
            'controller_name' => 'InventarioController',
        ]);
    }

    /**
     * @Route("/inventario/processar", name="inventario_processar")
     */
    public function getInventory(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $dbh = ibase_connect($this->host, $this->username, $this->password, $this->charset);
            $sql = 'update produtos a
                      set  a.DATA_CONT_EST = ?, a.QUANTIDADE = ?
                      where a.codigo = ?';
            $sth = ibase_prepare($dbh, $sql);
            $url = $request->request->get('url');
            $arquivo = fopen($url, "r+");
            $contagem = array();
            $codigosNaoEncontrados = array();
            while (!feof($arquivo)) {
                $line = trim(fgets($arquivo));
                if (!array_key_exists($line, $contagem)) {
                    $contagem[$line] = 1;
                } else {
                    $contagem[$line] = $contagem[$line] + 1;
                }
            }
            fclose($arquivo);
            try {
                foreach ($contagem as $prod => $qtdInv) {
                    $retorno = ibase_execute($sth, date('Y-m-d'), $qtdInv, $prod);
                    if (is_bool($retorno) === true) {
                        $codigosNaoEncontrados[$prod] = $qtdInv;
                    }
                }
            } catch (\Exception $e) {
                var_dump($e);
            } finally {
                ibase_free_query($sth);
                ibase_close($dbh);
            }
            $inventario = array();
            $inventario["contados"] = count($contagem);
            $inventario["naoEncontrados"] = $codigosNaoEncontrados;

            return new JsonResponse($inventario, 200);
        }
        return new JsonResponse("Not Ajax", 200);
    }
}
