<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashBoardController extends AbstractController
{
    private $host = 'localhost:C:\\Forti Up\\DADOS\\AOPESCADOR.FDB';
    private $username = 'SYSDBA';
    private $password = 'masterkey';

    /**
     * @Route("/dashboard/faturamento/ajax", name="_dashboard_faturamento")
     *
     */
    public function indexAction(Request $request)
    {

        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $mes = $request->request->get('mes');
            $ano = $request->request->get('ano');
            $tpData = $request->request->get('tpData');
            if ($ano > 0 and $mes > 0) {
                $dbh = ibase_connect($this->host, $this->username, $this->password);
                if ($tpData == 'DF') {
                    $stmt = '
                            select extract(day from vendas.data) as dia, sum (total) as total from (
                            
                            select A.DATA,
                                   case C.TIPO_PG
                                     when \'CHV\' then \'CHEQUE\'
                                     when \'DIN\' then \'DINHEIRO\'
                                     when \'CTC\' then \'CARTÃO CRÉDITO\'
                                     when \'CTD\' then \'CARTÃO DÉBITO\'
                                   end as TIPO_PG,
                            
                                   C.TOTAL
                            from CUPOM_VEND A, MOV_CAIXA C
                            where
                                  C.TIPO_PG <> \'TRC\' and
                                  A.ID_CUPOM_VEND = C.ID_ORIGEM and
                                  A.CUPOM_CANCELADO = \'N\'
                                  and extract(month from a.data) = ' . $mes . '
                                  and extract(year from a.data) = ' . $ano . '
                                  union all
                                  select
                            
                                ped.data,
                                \'C2\' tipo_pg,
                            
                                ped.total2 as total
                            from
                                pedido ped
                            where
                               extract(month from ped.data) = ' . $mes . '
                               and extract(year from ped.data) = ' . $ano . '
                               and ped.statuspag = \'N\'
                               and ped.tipo_pedido = \'P\'
                            
                               union all
                            
                               select
                                A.DATASAIDA as DATA,
                               \'NF\' as TIPO_PG,
                                   case B.CODIGO
                                     when \'5.102\' then A.TOTAL
                                     when \'6.102\' then A.TOTAL
                                     when \'1.202\' then (A.TOTAL * -1)
                                     when \'2.202\' then (A.TOTAL * -1)
                                   end as TOTAL
                            from NOTAFISCAL A, NATOP B
                            where A.ID_NATOP = B.ID_NATOP and
                                  B.CODIGO in (\'5.102\', \'6.102\', \'1.202\', \'2.202\') and
                                  A.STATUS = \'S\' and
                                  extract (month from A.DATASAIDA) = ' . $mes . ' and
                                  extract (year from A.DATASAIDA) = ' . $ano . ' and
                                  a.nome_arq_nfe is not null
                            
                                  ) as vendas group by extract(day from vendas.data)  ';
                } else {
                    $stmt = '
                            select extract(day from vendas.data) as dia, sum (total) as total from (
                            
                            select e.DATA,
                                   case C.TIPO_PG
                                     when \'CHV\' then \'CHEQUE\'
                                     when \'DIN\' then \'DINHEIRO\'
                                     when \'CTC\' then \'CARTÃO CRÉDITO\'
                                     when \'CTD\' then \'CARTÃO DÉBITO\'
                                   end as TIPO_PG,
                            
                                   C.TOTAL
                            from CUPOM_VEND A, MOV_CAIXA C, pedido e
                            where 1=1
                                  and C.TIPO_PG <> \'TRC\'
                                  and A.ID_CUPOM_VEND = C.ID_ORIGEM
                                  and A.CUPOM_CANCELADO = \'N\'
                                  and e.id_caixa = a.id_caixa
                                  and e.id_cupom = a.id_cupom_vend                                  
                                  and extract(month from e.data) = ' . $mes . '
                                  and extract(year from e.data) = ' . $ano . '
                                  union all
                                  select
                            
                                ped.data,
                                \'C2\' tipo_pg,
                            
                                ped.total2 as total
                            from
                                pedido ped
                            where
                               extract(month from ped.data) = ' . $mes . '
                               and extract(year from ped.data) = ' . $ano . '
                               and ped.statuspag = \'N\'
                               and ped.tipo_pedido = \'P\'
                            
                               union all
                            
                               select
                                A.DATASAIDA as DATA,
                               \'NF\' as TIPO_PG,
                                   case B.CODIGO
                                     when \'5.102\' then A.TOTAL
                                     when \'6.102\' then A.TOTAL
                                     when \'1.202\' then (A.TOTAL * -1)
                                     when \'2.202\' then (A.TOTAL * -1)
                                   end as TOTAL
                            from NOTAFISCAL A, NATOP B
                            where A.ID_NATOP = B.ID_NATOP and
                                  B.CODIGO in (\'5.102\', \'6.102\', \'1.202\', \'2.202\') and
                                  A.STATUS = \'S\' and
                                  extract (month from A.DATASAIDA) = ' . $mes . ' and
                                  extract (year from A.DATASAIDA) = ' . $ano . ' and
                                  a.nome_arq_nfe is not null
                            
                                  ) as vendas group by extract(day from vendas.data)  ';

                }
                $sth = ibase_query($dbh, $stmt);
                $arrayDia = array();
                $arrayTotal = array();
                while ($row = ibase_fetch_object($sth)) {
                    $arrayDia[] = $row->DIA;
                    $arrayTotal[] = $row->TOTAL;
                }

                ibase_free_result($sth);
                ibase_close($dbh);
                return new JsonResponse(array('dia' => $arrayDia, 'total' => $arrayTotal));


            }
        }
        return new Response('This is not ajax!', 400);

    }

    /**
     * @Route("/dashboard/faturamento_por_tipo_pagamento/ajax",name="_dashboard_fat_por_tipo_pag")
     *
     */
    public function fatPorTipoPagAction(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $mes = $request->request->get('mes');
            $ano = $request->request->get('ano');
            if ($ano > 0 and $mes > 0) {
                $dbh = ibase_connect($this->host, $this->username, $this->password);
                $stmt = '
                            select
                                case C.TIPO_PG
                                when \'CHV\' then \'CHEQUE\'
                                when \'DIN\' then \'DINHEIRO\'
                                when \'CTC\' then \'CARTÃO CRÉDITO\'
                                when \'CTD\' then \'CARTÃO DÉBITO\'
                                end as TIPO_PG,
                                sum(C.TOTAL) as total
                            from CUPOM_VEND A, MOV_CAIXA C
                            where
                                C.TIPO_PG <> \'TRC\' and
                                A.ID_CUPOM_VEND = C.ID_ORIGEM and
                                A.CUPOM_CANCELADO = \'N\'
                                and extract(month from a.data) = ' . $mes . '
                                and extract(year from a.data) = ' . $ano . '
                                group by c.tipo_pg
                            
                            
                            union all
                            
                            select
                            
                                \'C2\' tipo_pg,
                                sum(ped.total2) as total
                            from
                                pedido ped
                            where
                                extract(month from ped.data) =' . $mes . '
                                and extract(year from ped.data) = ' . $ano . '
                                and ped.statuspag = \'N\'
                                and ped.tipo_pedido = \'P\'
                                group by TIPO_PG
                            
                            
                            union all
                            
                            select
                            \'NF\' as TIPO_PG,
                            sum(case B.CODIGO
                            when \'5.102\' then A.TOTAL
                            when \'6.102\' then A.TOTAL
                            when \'1.202\' then (A.TOTAL * -1)
                            when \'2.202\' then (A.TOTAL * -1)
                            end) as TOTAL
                            from NOTAFISCAL A, NATOP B
                            where A.ID_NATOP = B.ID_NATOP and
                            B.CODIGO in (\'5.102\', \'6.102\', \'1.202\', \'2.202\') and
                            A.STATUS = \'S\' and
                            extract (month from A.DATASAIDA) = ' . $mes . ' and
                            extract (year from A.DATASAIDA) = ' . $ano . ' and
                            a.nome_arq_nfe is not null
                            group by tipo_pg
                                                        
                                    ';
                $sth = ibase_query($dbh, $stmt);
                $arrayTipo = array();
                $arrayTotal = array();
                while ($row = ibase_fetch_object($sth)) {
                    $arrayTipo[] = trim($row->TIPO_PG, ' ');
                    $arrayTotal[] = array('name' => TRIM($row->TIPO_PG, ' '), 'value' => $row->TOTAL);
                }

                ibase_free_result($sth);
                ibase_close($dbh);
                return new JsonResponse(array('tipopg' => $arrayTipo, 'total' => $arrayTotal));


            }
        }
        return new Response('This is not ajax!', 400);
    }

    /**
     * @Route("/dashboard/faturamento_por_categoria/ajax",name="_dashboard_fat_categoria")
     *
     */
    public function fatPorGrupo(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $mes = $request->request->get('mes');
            $ano = $request->request->get('ano');
            $tpData = $request->request->get('tpData');
            if ($ano > 0 and $mes > 0) {
                $dbh = ibase_connect($this->host, $this->username, $this->password, '', '100', '3');
                $stmt =
                    'select x.pai, sum(x.total) as total from (
                        select
                              a.data,
                                (select b.descricao from grupoprod b where b.id_grupoprod = left(h.id_grupoprod,2)) as pai,
                               g.codigo,
                               g.descricao,
                               f.total
                        
                        from  itemcupom_vend f, produtos g, grupoprod h, cupom_vend a
                        where
                              1=1
                              and h.id_grupoprod = g.id_grupoprod
                              and f.id_produto = g.id_produto
                              and a.id_cupom_vend = f.id_cupom_vend
                              and A.CUPOM_CANCELADO = \'N\'
                              and extract(month from a.data) = ' . $mes . '
                              and extract(year from a.data) = ' . $ano . '
                                       )
                    as x
                    group by x.pai';
                $sth = ibase_query($dbh, $stmt);
                $arrayTotal = array();
                while ($row = ibase_fetch_object($sth)) {

                    $arrayTotal[] = array('name' => $row->PAI, 'value' => $row->TOTAL);
                }

                ibase_free_result($sth);
                ibase_close($dbh);
                return new JsonResponse(array('total' => $arrayTotal));
            }
        }
        return new Response('This is not ajax!', 400);
    }



    /**
     * @Route("/dashboard/faturamento/total",name="total_mes")
     */

    public function movimentoMesAction(Request $request)
    {
        return $this->render('default/page_principal.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
        ]);
    }
}
