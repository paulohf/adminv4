<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class AjusteSaldosEstoqueController extends AbstractController
{
    private $host = 'localhost:C:\\Forti Up\\DADOS\\AOPESCADOR.FDB';
    private $username = 'SYSDBA';
    private $password = 'masterkey';

    /**
     * @Route("/ajuste_saldos_estoque", name="ajuste_saldos_estoque")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/page_ajuste_saldos_estoque.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
        ]);
    }


    /**
     * @Route("/estoque/saldos/ajax",name="_estoque_saldos")
     */
    public function estoqueSaldoAction(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {

            $dbh = ibase_connect($this->host, $this->username, $this->password);
            $stmt = 'select
                        a.id_produto,
                        a.codigo,
                        a.descricao,
                        a.quantidade,
                        a.precocompra,
                        a.preco_custo
                    from
                        produtos a
                    where
                        1=1
                        and a.status_exclusao is null or a.status_exclusao = \'N\'
                     order by a.descricao   
                    ';
            $sth = ibase_query($dbh, $stmt);
            $arrayProdutos = array();
            while ($row = ibase_fetch_object($sth)) {
                $arrayProdutos[] = array(
                    'codigo' => $row->CODIGO,
                    'idproduto' => $row->ID_PRODUTO,
                    'quantidade' => $row->QUANTIDADE,
                    'precocompra' => $row->PRECOCOMPRA,
                    'precocusto' => $row->PRECO_CUSTO,
                    'descricao' => utf8_encode($row->DESCRICAO)
                );

            }

            ibase_free_result($sth);
            ibase_close($dbh);
            return new JsonResponse(array('produtos' => $arrayProdutos));
        }
        return new Response('This is not ajax!', 400);
    }
}
