<?php

namespace App\Controller;

use App\Entity\ProdutoVirtual;
use App\Form\ProdutosLojaType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Automattic\WooCommerce\Client;

class IntegraLojaController extends AbstractController
{

    private $host = 'localhost:C:\\Forti Up\\DADOS\\AOPESCADOR.FDB';
    private $username = 'SYSDBA';
    private $password = 'masterkey';

    /**
     * @Route("/inventario/loja", name="inventario_loja")
     */
    public function inventarioLoja(Request $request)
    {
        try {
            $woocommerce = new Client(
                'http://miguelpescador.com.br/loja',
                'ck_62303baaa59ac3377ff2e4e24ced7805497f1c32',
                'cs_c7fe2e7f283a8022561d0c2b8b75032113a77d85',
                [
                    'version' => 'wc/v3',
                    'timeout' => 120,
                ]
            );

            $hasProduct = true;
            $page = 1;
            $resultados = array();
            while ($hasProduct) {
                $products = $woocommerce->get('products', $parameters = ['per_page' => 100, 'page' => $page,
                    'status' => 'publish', 'order' => 'asc', 'orderby' => 'title']);
                if (count($products) <= 0) {
                    $hasProduct = false;
                } else {
                    for ($i = 0; $i < count($products); $i++) {
                        // dump($products[$i]->sku);
                        $produto = new ProdutoVirtual();
                        $produto->setCodigo($products[$i]->sku);
                        $produto->setDescricao($products[$i]->name);
                        if ($products[$i]->stock_quantity <= 0) {
                            $qty = 0;
                        } else {
                            $qty = $products[$i]->stock_quantity;
                        }
                        $produto->setSaldo($qty);
                        $produto->setQtyInventario($qty);

                        array_push($resultados, $produto);
                    }
                }
                $page++;
            }

            $form = $this->createForm(ProdutosLojaType::class, array('produtos' => $resultados));
            $form->handleRequest($request);

            if ($form->isSubmitted()) {
                if ($form->isValid()) {

                    $sqlUpdate = "update produtos set quantidade = ? where codigo = ?
                              and (status_exclusao = 'N' or status_exclusao is null)";

                    $dbh = ibase_connect($this->host, $this->username, $this->password);
                    $queryUpdate = ibase_prepare($dbh, $sqlUpdate);

                    $inventario = $form->getData();
                    $produtos = array();
                    $produtos = $inventario['produtos'];

                    for ($i = 0; $i < count($produtos); $i++) {

                        if ($produtos[$i]->getSaldo() <> $produtos[$i]->getQtyInventario()) {

                            $linhasAfetadas = ibase_execute($queryUpdate, $produtos[$i]->getQtyInventario(),$produtos[$i]->getCodigo());

                        }

                    }

                    ibase_close($dbh);
                }
            }

        } catch (Exception $ex) {

        };


        return $this->render("default/integra_loja/page_inventario_loja.twig", array("produtos" => $resultados, "form" => $form->createView()));
    }

    /**
     * @Route("/atualiza_saldo/loja", name="atualiza_saldo_loja")
     */
    public function index()
    {
        return $this->render('default/integra_loja/page_integra_loja.twig');
    }


    /**
     * @Route("/v1/integracao/loja", name="v1_integracao_loja")
     */

    public function integraLoja(Request $request)
    {
        $response = new StreamedResponse();
        $response->headers->set('Content-Type', 'text/event-stream');
        $response->headers->set('Cache-Control', 'no-cache');

        $response->setCallback(function () {
            $this->sincronize();
        });
        $response->send();

        return $response;
    }

    public function sincronize()
    {
        ini_set('max_execution_time', 0);

        try {


            $woocommerce = new Client(
                'http://miguelpescador.com.br/loja',
                'ck_62303baaa59ac3377ff2e4e24ced7805497f1c32',
                'cs_c7fe2e7f283a8022561d0c2b8b75032113a77d85',
                [
                    'version' => 'wc/v3',
                    'timeout' => 60,
                ]
            );
            echo '5#Iniciando leitura de produtos da loja virtual.';
            ob_flush();
            flush();
            $hasProduct = true;
            $page = 1;
            $resultProducts = array();
            while ($hasProduct) {
                $products = $woocommerce->get('products', $parameters = ['per_page' => 100, 'page' => $page, 'status' => 'publish']);
                if (count($products) <= 0) {
                    $hasProduct = false;
                } else {
                    $resultProducts = array_merge($resultProducts, $products);
                    echo '20#Produtos lidos ' . count($resultProducts);
                    ob_flush();
                    flush();
                }
                $page++;
            }
            echo '30#Verificando produtos a serem atualizados.';
            ob_flush();
            flush();
            $dbh = ibase_connect($this->host, $this->username, $this->password);
            $stmt = '';

            echo '50#Total de produtos lidos da loja virtual  ' . count($resultProducts);
            ob_flush();
            flush();
            $updateArray = array();
            $totalProdutosLojaVirtual = count($resultProducts);
            for ($i = 0; $i < $totalProdutosLojaVirtual; $i++) {

                $productId = $resultProducts[$i]->sku;
                if (strlen($productId) > 0) {
                    $quantidade = 0;
                    $preco = 0;
                    $stmt = 'select p.precovenda5, p.quantidade from produtos p where p.codigo = \'' . $productId . '\'';
                    $sth = ibase_query($dbh, $stmt);

                    while ($row = ibase_fetch_object($sth)) {
                        if ($row->PRECOVENDA5 <> $resultProducts[$i]->price or $row->QUANTIDADE <> $resultProducts[$i]->stock_quantity) {

                            if ($row->QUANTIDADE < 0) {
                                $quantidade = 0;
                            } else {
                                $quantidade = $row->QUANTIDADE;
                            }
                            $preco = $row->PRECOVENDA5;
                            array_push($updateArray, array('id' => $resultProducts[$i]->id, 'stock_quantity' => $quantidade,
                                'regular_price' => $preco, 'manage_stock' => 'true'));
                        }
                    }
                }
                $progresso = round(((40 * $i) / $totalProdutosLojaVirtual) + 50);
                echo $progresso . '#' . 'Preparando registro ' . $i . ' de ' . $totalProdutosLojaVirtual . '#';
                ob_flush();
                flush();

            }
            echo '50# quantidade de produtos lidos ' . $i;
            ob_flush();
            flush();
            $woocommerce->post('products/batch', array('update' => $updateArray));

            echo '100# Loja virtual atualizada. ';
            ob_flush();
            flush();


        } catch (Exception $e) {
            ibase_free_result($sth);
            ibase_close($dbh);
            echo '0#' . 'Error: ' . $e->getMessage();
            ob_flush();
            flush();

        };

    }


}
