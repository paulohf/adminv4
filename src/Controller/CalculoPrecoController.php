<?php

namespace App\Controller;

use App\Entity\NotaFiscal;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class CalculoPrecoController extends AbstractController
{

    private $host = 'localhost:C:\\Forti Up\\DADOS\\AOPESCADOR.FDB';
    private $username = 'SYSDBA';
    private $password = 'masterkey';
    private $charset = 'ISO8859_9';

    /**
     * @Route("/notafiscalentrada/", name="NotaFiscalEntrada")
     */
    public function getNotaFiscalEntrada(Request $request)
    {
        $dbh = ibase_connect($this->host, $this->username, $this->password, 'ISO8859_1');
        $stmt = 'select a.id_notafiscal, a.codigo, a.nome_vinculo, a.data
                  from notafiscal a
                  where a.tipo_notafiscal = \'E\'
                  and a.data > dateadd (-180 day to current_date)
                  order by a.data desc';
        $sth = ibase_query($dbh, $stmt);

        $resultado = array();
        while ($row = ibase_fetch_object($sth)) {
            $nota = new NotaFiscal();
            $nota->setIdNF($row->ID_NOTAFISCAL);
            $nota->setNota($row->CODIGO);
            $nota->setFornecedor(utf8_encode($row->NOME_VINCULO));
            $nota->setData($row->DATA);
            array_push($resultado, $nota);

        }
        ibase_free_result($sth);
        ibase_close($dbh);
        return $this->render('default/estoque/page_nf_entrada.twig', array('notas' => $resultado));

    }


    /**
     * @Route("/CalculoPreco", name="CalculaPreco", methods={"POST"})
     */
    public function indexAction(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $nfId = "";
            $nfId = $request->request->get('nfId');

            $dbh = ibase_connect($this->host, $this->username, $this->password, $this->charset);
            $stmt = '
                    select 
                        A.ID_PRODUTO,
                        A.DESCRICAO,
                        a.PRECOCOMPRA,
                        A.PRECO_CUSTO,
                        a.margem1,
                        A.PRECOVENDA,
                        a.margem2,
                        A.PRECOVENDA2,
                        a.margem3,
                        A.PRECOVENDA3,
                        a.margem4,
                        A.PRECOVENDA4,
                        a.margem5,
                        A.PRECOVENDA5,
                        b.quantidade,
                        a.data_venc,
                        b.valor,
                        b.total,
                        b.IPI_VIPI,
                        a.codigo,
                        b.icms_vicms_st,
                        b.fator_conv_un,
                        b.tipo_calculo_conv_un, 
                        b.tipo_aliquota,
                        b.icms
                     from 
                      PRODUTOS A, 
                      item_notafiscal b
                     where
                      1=1
                      and a.id_produto = b.id_produto 
                      and b.id_notafiscal = ' . $nfId . '
                     order by b.id_itemnotafiscal
                
                ';
            $sth = ibase_query($dbh, $stmt);
            $produtosAux = array();
            $produto = array();
            while ($row = ibase_fetch_object($sth)) {
                $produto['descricao'] = utf8_encode($row->DESCRICAO);
                array_push($produtosAux,$produto);
            }
            $produtos=array();
            $produtos['data']=$produtosAux;
            ibase_free_result($sth);
            ibase_close($dbh);
            return new JsonResponse(array($produtosAux));

        }
        return new Response('This is not ajax!', 400);


    }

    /**
     * @Route("/CalculoNF/", name="CalculoNF")
     */
    public function calcPrice(){
        return $this->render('default/estoque/calculo_preco.twig', array());
    }

}
