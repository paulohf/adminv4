<?php

namespace App\Entity;

use App\Repository\MapaClienteFornecedorEndRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MapaClienteFornecedorEndRepository::class)
 */
class MapaClienteFornecedorEnd
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $cep;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $cidade;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $uf;

    /**
     * @ORM\Column(type="string", length=70)
     */
    private $endereco;

    /**
     * @ORM\OneToOne(targetEntity=MapaClienteFornecedor::class, inversedBy="endereco", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $clienteFornecedor;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $complemento;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $numero;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $bairro;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCep(): ?int
    {
        return $this->cep;
    }

    public function setCep(int $cep): self
    {
        $this->cep = $cep;

        return $this;
    }

    public function getCidade(): ?string
    {
        return $this->cidade;
    }

    public function setCidade(string $cidade): self
    {
        $this->cidade = $cidade;

        return $this;
    }

    public function getUf(): ?string
    {
        return $this->uf;
    }

    public function setUf(string $uf): self
    {
        $this->uf = $uf;

        return $this;
    }

    public function getEndereco(): ?string
    {
        return $this->endereco;
    }

    public function setEndereco(string $endereco): self
    {
        $this->endereco = $endereco;

        return $this;
    }

    public function getClienteFornecedor(): ?MapaClienteFornecedor
    {
        return $this->clienteFornecedor;
    }

    public function setClienteFornecedor(MapaClienteFornecedor $clienteFornecedor): self
    {
        $this->clienteFornecedor = $clienteFornecedor;

        return $this;
    }

    public function getComplemento(): ?string
    {
        return $this->complemento;
    }

    public function setComplemento(?string $complemento): self
    {
        $this->complemento = $complemento;

        return $this;
    }

    public function getNumero(): ?string
    {
        return $this->numero;
    }

    public function setNumero(?string $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getBairro(): ?string
    {
        return $this->bairro;
    }

    public function setBairro(?string $bairro): self
    {
        $this->bairro = $bairro;

        return $this;
    }
}
