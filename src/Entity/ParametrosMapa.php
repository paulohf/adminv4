<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="parametrosMapa")
 */

class ParametrosMapa
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $mesAbertoVendaArma;

    /**
     * @ORM\Column(type="integer")
     */
    private $anoAbertovendaArma;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $ultMovArma;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $ultSaldoArma;

    /**
     * @return mixed
     */
    public function getMesAbertoVendaArma()
    {
        return $this->mesAbertoVendaArma;
    }

    /**
     * @param mixed $mesAbertoVendaArma
     */
    public function setMesAbertoVendaArma($mesAbertoVendaArma)
    {
        $this->mesAbertoVendaArma = $mesAbertoVendaArma;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAnoAbertovendaArma()
    {
        return $this->anoAbertovendaArma;
    }

    /**
     * @param mixed $anoAbertovendaArma
     */
    public function setAnoAbertovendaArma($anoAbertovendaArma)
    {
        $this->anoAbertovendaArma = $anoAbertovendaArma;
    }

    public function getUltMovArma(): ?\DateTimeInterface
    {
        return $this->ultMovArma;
    }

    public function setUltMovArma(?\DateTimeInterface $ultMovArma): self
    {
        $this->ultMovArma = $ultMovArma;

        return $this;
    }

    public function getUltSaldoArma(): ?\DateTimeInterface
    {
        return $this->ultSaldoArma;
    }

    public function setUltSaldoArma(?\DateTimeInterface $ultSaldoArma): self
    {
        $this->ultSaldoArma = $ultSaldoArma;

        return $this;
    }





}