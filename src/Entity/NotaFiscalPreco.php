<?php

namespace App\Entity;

class NotaFiscalPreco
{
    private $idProduto;
    private $codigo;
    private $descricao;
    private $preco1;
    private $preco2;
    private $preco3;
    private $preco4;
    private $preco5;
    private $icmsOrigem;
    private $icmsDestino;
    private $margem1;
    private $margem2;
    private $margem3;
    private $margem4;
    private $margem5;
    private $prcusto;
    private $prcompra;
    private $icmsST;
    private $ipi;

    /**
     * @return mixed
     */
    public function getIdProduto()
    {
        return $this->idProduto;
    }

    /**
     * @param mixed $idProduto
     */
    public function setIdProduto($idProduto)
    {
        $this->idProduto = $idProduto;
    }

    /**
     * @return mixed
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @param mixed $codigo
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    /**
     * @return mixed
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param mixed $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return mixed
     */
    public function getPreco1()
    {
        return $this->preco1;
    }

    /**
     * @param mixed $preco1
     */
    public function setPreco1($preco1)
    {
        $this->preco1 = $preco1;
    }

    /**
     * @return mixed
     */
    public function getPreco2()
    {
        return $this->preco2;
    }

    /**
     * @param mixed $preco2
     */
    public function setPreco2($preco2)
    {
        $this->preco2 = $preco2;
    }

    /**
     * @return mixed
     */
    public function getPreco3()
    {
        return $this->preco3;
    }

    /**
     * @param mixed $preco3
     */
    public function setPreco3($preco3)
    {
        $this->preco3 = $preco3;
    }

    /**
     * @return mixed
     */
    public function getPreco4()
    {
        return $this->preco4;
    }

    /**
     * @param mixed $preco4
     */
    public function setPreco4($preco4)
    {
        $this->preco4 = $preco4;
    }

    /**
     * @return mixed
     */
    public function getPreco5()
    {
        return $this->preco5;
    }

    /**
     * @param mixed $preco5
     */
    public function setPreco5($preco5)
    {
        $this->preco5 = $preco5;
    }

    /**
     * @return mixed
     */
    public function getIcmsOrigem()
    {
        return $this->icmsOrigem;
    }

    /**
     * @param mixed $icmsOrigem
     */
    public function setIcmsOrigem($icmsOrigem)
    {
        $this->icmsOrigem = $icmsOrigem;
    }

    /**
     * @return mixed
     */
    public function getIcmsDestino()
    {
        return $this->icmsDestino;
    }

    /**
     * @param mixed $icmsDestino
     */
    public function setIcmsDestino($icmsDestino)
    {
        $this->icmsDestino = $icmsDestino;
    }

    /**
     * @return mixed
     */
    public function getMargem1()
    {
        return $this->margem1;
    }

    /**
     * @param mixed $margem1
     */
    public function setMargem1($margem1)
    {
        $this->margem1 = $margem1;
    }

    /**
     * @return mixed
     */
    public function getMargem2()
    {
        return $this->margem2;
    }

    /**
     * @param mixed $margem2
     */
    public function setMargem2($margem2)
    {
        $this->margem2 = $margem2;
    }

    /**
     * @return mixed
     */
    public function getMargem3()
    {
        return $this->margem3;
    }

    /**
     * @param mixed $margem3
     */
    public function setMargem3($margem3)
    {
        $this->margem3 = $margem3;
    }

    /**
     * @return mixed
     */
    public function getMargem4()
    {
        return $this->margem4;
    }

    /**
     * @param mixed $margem4
     */
    public function setMargem4($margem4)
    {
        $this->margem4 = $margem4;
    }

    /**
     * @return mixed
     */
    public function getMargem5()
    {
        return $this->margem5;
    }

    /**
     * @param mixed $margem5
     */
    public function setMargem5($margem5)
    {
        $this->margem5 = $margem5;
    }

    /**
     * @return mixed
     */
    public function getPrcusto()
    {
        return $this->prcusto;
    }

    /**
     * @param mixed $prcusto
     */
    public function setPrcusto($prcusto)
    {
        $this->prcusto = $prcusto;
    }

    /**
     * @return mixed
     */
    public function getPrcompra()
    {
        return $this->prcompra;
    }

    /**
     * @param mixed $prcompra
     */
    public function setPrcompra($prcompra)
    {
        $this->prcompra = $prcompra;
    }

    /**
     * @return mixed
     */
    public function getIcmsST()
    {
        return $this->icmsST;
    }

    /**
     * @param mixed $icmsST
     */
    public function setIcmsST($icmsST)
    {
        $this->icmsST = $icmsST;
    }

    /**
     * @return mixed
     */
    public function getIpi()
    {
        return $this->ipi;
    }

    /**
     * @param mixed $ipi
     */
    public function setIpi($ipi)
    {
        $this->ipi = $ipi;
    }



}