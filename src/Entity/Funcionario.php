<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="funcionario")
 */
class Funcionario
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string",length=50)
     * @Assert\Length(max=50,
     *     maxMessage = "A descrição não pode ser maior que {{ limit }} characters")
     */
    private $nome;
    /**
     * @ORM\Column(type="string",length=20,nullable=true)
     *
     */
    private $rg;
    /**
     * @ORM\Column(type="string",length=20,nullable=true)
     */
    private $cpf;
    /**
     * @ORM\Column(type="boolean")
     */
    private $socio;
    /**
     * @ORM\Column(type="text",nullable=true)
     */
    private $obs;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Retirada",mappedBy="funcionario")
     */
    private $retirada;

    /**
     * @return mixed
     */
    public function getRetirada()
    {
        return $this->retirada;
    }

    /**
     * @param mixed $retirada
     */
    public function setRetirada($retirada)
    {
        $this->retirada = $retirada;
    }

    /**
     * @return mixed
     */
    public function getObs()
    {
        return $this->obs;
    }

    /**
     * @param mixed $obs
     */
    public function setObs($obs)
    {
        $this->obs = $obs;
    }


    /**
     * @return mixed
     */
    public function getSocio()
    {
        return $this->socio;
    }

    /**
     * @param mixed $socio
     */
    public function setSocio($socio)
    {
        $this->socio = $socio;
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getRg()
    {
        return $this->rg;
    }

    /**
     * @param mixed $rg
     */
    public function setRg($rg)
    {
        $this->rg = $rg;
    }

    /**
     * @return mixed
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @param mixed $cpf
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
    }




}