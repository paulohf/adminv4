<?php


namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;


class Etiqueta
{
    private $idProduto;
    /**
     * @Assert\Length(max=11,
     *     maxMessage="O Código não pode ser maior que {{ limit }} characters")
     */
    private $codigo;
    private $descricao;
    /**
     * @Assert\Length(max=60,
     *     maxMessage = "A descrição não pode ser maior que {{ limit }} characters")
     */
    private $descEtiqueta;
    private $data;
    private $qtde;
    private $preco;

    /**
     * @return mixed
     */
    public function getIdProduto()
    {
        return $this->idProduto;
    }

    /**
     * @param mixed $idProduto
     */
    public function setIdProduto($idProduto): void
    {
        $this->idProduto = $idProduto;
    }

    /**
     * @return mixed
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @param mixed $codigo
     */
    public function setCodigo($codigo): void
    {
        $this->codigo = $codigo;
    }

    /**
     * @return mixed
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param mixed $descricao
     */
    public function setDescricao($descricao): void
    {
        $this->descricao = $descricao;
    }

    /**
     * @return mixed
     */
    public function getDescEtiqueta()
    {
        return $this->descEtiqueta;
    }

    /**
     * @param mixed $descEtiqueta
     */
    public function setDescEtiqueta($descEtiqueta): void
    {
        $this->descEtiqueta = $descEtiqueta;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data): void
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getQtde()
    {
        return $this->qtde;
    }

    /**
     * @param mixed $qtde
     */
    public function setQtde($qtde): void
    {
        $this->qtde = $qtde;
    }

    /**
     * @return mixed
     */
    public function getPreco()
    {
        return $this->preco;
    }

    /**
     * @param mixed $preco
     */
    public function setPreco($preco): void
    {
        $this->preco = $preco;
    }



}