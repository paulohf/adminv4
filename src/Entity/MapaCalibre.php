<?php

namespace App\Entity;

use App\Repository\MapaCalibreRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MapaCalibreRepository::class)
 */
class MapaCalibre
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $descricao;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $tipoCalibre;



    public function __construct()
    {
        $this->mapaEstoques = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescricao(): ?string
    {
        return $this->descricao;
    }

    public function setDescricao(string $descricao): self
    {
        $this->descricao = $descricao;

        return $this;
    }

    public function getTipoCalibre(): ?string
    {
        return $this->tipoCalibre;
    }

    public function setTipoCalibre(string $tipoCalibre): self
    {
        $this->tipoCalibre = $tipoCalibre;

        return $this;
    }

    public function __toString()
    {
        return (string) $this->getId();
    }


}
