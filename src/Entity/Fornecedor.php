<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="fornecedor")
 */
class Fornecedor
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string",length=45)
     * @Assert\Length(max=45,maxMessage="Número de caracters ultrapassa o limite definido que é de {{ limit }} caracteres!")
     */
    private $nome;


    /**
     * @ORM\Column(type="string",length=45)
     * @Assert\Length(max=45,maxMessage="Número de caracters ultrapassa o limite definido que é de {{ limit }} caracteres!")
     */
    private $endereco;
    /**
     * @ORM\Column(type="string",length=15,nullable=true)
     * @Assert\Length(max=15,maxMessage="Número de caracters ultrapassa o limite definido que é de {{ limit }} caracteres!")
     */
    private $telefone1;
    /**
     * @ORM\Column(type="string",length=15,nullable=true)
     * @Assert\Length(max=15,maxMessage="Número de caracters ultrapassa o limite definido que é de {{ limit }} caracteres!")
     */
    private $telefone2;
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $observacao;

    /**
     * @ORM\Column(type="integer")
     */
    private $cnpj;



    public function __construct() {
        $this->idTitulos = new ArrayCollection();
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param mixed $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

    /**
     * @return mixed
     */
    public function getTelefone1()
    {
        return $this->telefone1;
    }

    /**
     * @param mixed $telefone1
     */
    public function setTelefone1($telefone1)
    {
        $this->telefone1 = $telefone1;
    }

    /**
     * @return mixed
     */
    public function getTelefone2()
    {
        return $this->telefone2;
    }

    /**
     * @param mixed $telefone2
     */
    public function setTelefone2($telefone2)
    {
        $this->telefone2 = $telefone2;
    }

    /**
     * @return mixed
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * @param mixed $observacao
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;
    }

    public function getCnpj(): ?int
    {
        return $this->cnpj;
    }

    public function setCnpj(int $cnpj): self
    {
        $this->cnpj = $cnpj;

        return $this;
    }






}
