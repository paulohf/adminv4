<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="municao",indexes={@ORM\Index(name="idproduto_idx", columns={"id_Produto"})})
 */
class Municao
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="integer")
     */
    private $idProduto;

    /**
     * @ORM\Column(type="text",length=20)
     */
    private $codigo;

    /**
     * @ORM\Column(type="string",length=70)
     */
    private $descricao;











}