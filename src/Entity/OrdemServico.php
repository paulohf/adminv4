<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="OrdemServico",indexes={
 *          @ORM\Index(name="idOrdemServico_idx", columns={"id"}),
 *
 *      })
 */
class OrdemServico
{


    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string",length=150)
     */
    private $produto;

    /**
     * @ORM\Column(type="string",length=255)
     */
    private $problema;

    /**
     * @ORM\Column(type="date")
     */
    private $data;


    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nome;


    /**
     * @ORM\Column(type="string",length=100)
     */
    private $telefone;

    /**
     * @ORM\Column(type="boolean")
     */
    private $urgente;

    /**
     * @ORM\Column(type="boolean")
     */
    private $orcamento;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status;

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getUrgente()
    {
        return $this->urgente;
    }

    /**
     * @param mixed $urgente
     */
    public function setUrgente($urgente)
    {
        $this->urgente = $urgente;
    }

    /**
     * @return mixed
     */
    public function getOrcamento()
    {
        return $this->orcamento;
    }

    /**
     * @param mixed $orcamento
     */
    public function setOrcamento($orcamento)
    {
        $this->orcamento = $orcamento;
    }


    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getTelefone()
    {
        return $this->telefone;
    }

    /**
     * @param mixed $telefone
     */
    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getProduto()
    {
        return $this->produto;
    }

    /**
     * @param mixed $produto
     */
    public function setProduto($produto)
    {
        $this->produto = $produto;
    }

    /**
     * @return mixed
     */
    public function getProblema()
    {
        return $this->problema;
    }

    /**
     * @param mixed $problema
     */
    public function setProblema($problema)
    {
        $this->problema = $problema;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }


}