<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="NotaFiscalMapa" ,indexes={
 *          @ORM\Index(name="idNotaFiscalMapa_idx", columns={"id"}),
 *
 *      }))
 */
class NotaFiscalMapa
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    private $id;

    /**
     * @ORM\Column(type="text",length=50)
     * @Assert\Length(max=60,
     *     maxMessage = "A descrição não pode ser maior que {{ limit }} characters")
     */
    private $nome;

    /**
     * @ORM\Column(type="text",length=11)
     */
    private $cpf;

    /**
     * @ORM\Column(type="text",length=50)
     */
    private $produto;

     /**
     * @ORM\Column(type="text",length=30)
      *
     */
    private $serial;

    /**
     * @ORM\Column(type="text",length=15)
     * @Assert\Length(max=15,
     *     maxMessage = "A descrição não pode ser maior que {{ limit }} characters")
     */
    private $fabricante;

    /**
     * @ORM\Column(type="date")
     */
    private $data;

    /**
     * @ORM\Column(type="text",length=100)
     * @Assert\Length(max=100,
     *     maxMessage = "O endereço não pode ser maior que {{ limit }} characters")
     */
    private $endereco;

    /**
     * @ORM\Column(type="integer")
     */
    private $notafiscal;

    /**
     * @return mixed
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param mixed $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

    /**
     * @return mixed
     */
    public function getNotafiscal()
    {
        return $this->notafiscal;
    }

    /**
     * @param mixed $notafiscal
     */
    public function setNotafiscal($notafiscal)
    {
        $this->notafiscal = $notafiscal;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @param mixed $cpf
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
    }

    /**
     * @return mixed
     */
    public function getProduto()
    {
        return $this->produto;
    }

    /**
     * @param mixed $produto
     */
    public function setProduto($produto)
    {
        $this->produto = $produto;
    }

    /**
     * @return mixed
     */
    public function getSerial()
    {
        return $this->serial;
    }

    /**
     * @param mixed $serial
     */
    public function setSerial($serial)
    {
        $this->serial = $serial;
    }

    /**
     * @return mixed
     */
    public function getFabricante()
    {
        return $this->fabricante;
    }

    /**
     * @param mixed $fabricante
     */
    public function setFabricante($fabricante)
    {
        $this->fabricante = $fabricante;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }






}


