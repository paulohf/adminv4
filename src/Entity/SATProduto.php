<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="sat_produto",indexes={@ORM\Index(name="idproduto_idx", columns={"id_Produto"})})
 */
Class SATProduto
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     *
     */
    private $idProduto;
    /**
     * @ORM\Column(type="datetime")
     */
    private $dataInclusao;
    private $codigo;
    private $descricao;
    private $origem;
    /**
     * @Assert\Length(
     *     min="8",
     *     minMessage="NCM menor do que {{ limit }} caracteres"
     * )
     *
     * @Assert\NotEqualTo(
     *     value="00000000",
     *     message="NCM não pode ser igual a {{ compared_value }}"
     * )
     */
    private $ncm;

    /**
     * @Assert\EqualTo(
     *     value="49",
     *     message="Valor diferente de {{ compared_value }}")
     */
    private $pis;
    /**
     * @Assert\EqualTo(
     *     value="49",
     *     message="Valor diferente de {{ compared_value }}")
     */
    private $cofins;
    private $simples_csosn;
    /**
     * @Assert\Choice({"TR", "FF","NN","II"}, message="Tipo de tributação {{ value }} é inválido")
     */
    private $tributacao;


    /**
     * @Assert\Choice({"18", "25", "12"}, message="Valor de ICMS {{ value }} é inválido")
     */
    private $icms;

    /**
     * @return mixed
     */
    public function getIdProduto()
    {
        return $this->idProduto;
    }

    /**
     * @param mixed $idProduto
     */
    public function setIdProduto($idProduto)
    {
        $this->idProduto = $idProduto;
    }

    /**
     * @return mixed
     */
    public function getDataInclusao()
    {
        return $this->dataInclusao;
    }

    /**
     * @param mixed $dataInclusao
     */
    public function setDataInclusao($dataInclusao)
    {
        $this->dataInclusao = $dataInclusao;
    }

    /**
     * @return mixed
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @param mixed $codigo
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    /**
     * @return mixed
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param mixed $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return mixed
     */
    public function getOrigem()
    {
        return $this->origem;
    }

    /**
     * @param mixed $origem
     */
    public function setOrigem($origem)
    {
        $this->origem = $origem;
    }

    /**
     * @return mixed
     */
    public function getNcm()
    {
        return $this->ncm;
    }

    /**
     * @param mixed $ncm
     */
    public function setNcm($ncm)
    {
        $this->ncm = $ncm;
    }

    /**
     * @return mixed
     */
    public function getPis()
    {
        return $this->pis;
    }

    /**
     * @param mixed $pis
     */
    public function setPis($pis)
    {
        $this->pis = $pis;
    }

    /**
     * @return mixed
     */
    public function getCofins()
    {
        return $this->cofins;
    }

    /**
     * @param mixed $cofins
     */
    public function setCofins($cofins)
    {
        $this->cofins = $cofins;
    }

    /**
     * @return mixed
     */
    public function getSimplesCsosn()
    {
        return $this->simples_csosn;
    }

    /**
     * @param mixed $simples_csosn
     */
    public function setSimplesCsosn($simples_csosn)
    {
        $this->simples_csosn = $simples_csosn;
    }

    /**
     * @return mixed
     */
    public function getTributacao()
    {
        return $this->tributacao;
    }

    /**
     * @param mixed $tributacao
     */
    public function setTributacao($tributacao)
    {
        $this->tributacao = $tributacao;
    }

    /**
     * @return mixed
     */
    public function getIcms()
    {
        return $this->icms;
    }

    /**
     * @param mixed $icms
     */
    public function setIcms($icms)
    {
        $this->icms = $icms;
    }


}