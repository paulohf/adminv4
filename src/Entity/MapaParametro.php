<?php

namespace App\Entity;

use App\Repository\MapaParametroRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MapaParametroRepository::class)
 */
class MapaParametro
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $mesAberto;

    /**
     * @ORM\Column(type="integer")
     */
    private $anoAberto;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dataUltProcSaldo;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dataUltProcMov;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMesAberto(): ?int
    {
        return $this->mesAberto;
    }

    public function setMesAberto(int $mesAberto): self
    {
        $this->mesAberto = $mesAberto;

        return $this;
    }

    public function getAnoAberto(): ?int
    {
        return $this->anoAberto;
    }

    public function setAnoAberto(int $anoAberto): self
    {
        $this->anoAberto = $anoAberto;

        return $this;
    }

    public function getDataUltProcSaldo(): ?\DateTimeInterface
    {
        return $this->dataUltProcSaldo;
    }

    public function setDataUltProcSaldo(?\DateTimeInterface $dataUltProcSaldo): self
    {
        $this->dataUltProcSaldo = $dataUltProcSaldo;

        return $this;
    }

    public function getDataUltProcMov(): ?\DateTimeInterface
    {
        return $this->dataUltProcMov;
    }

    public function setDataUltProcMov(?\DateTimeInterface $dataUltProcMov): self
    {
        $this->dataUltProcMov = $dataUltProcMov;

        return $this;
    }
}
